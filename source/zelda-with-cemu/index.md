title: CEMU 運行《薩爾達傳說 荒野之息》
date: 2017-04-08 00:07:29
---
![](https://wx1.sinaimg.cn/large/6d9bd6a5gy1feera0afdij21000lnhdt.jpg)

## 遊戲下載：

The Legend of Zelda - Breath of the Wild / Zelda no Densetsu - Breath of the Wild （Europe）

https://pan.baidu.com/s/1pLbgFph 
密碼（base64）: Ymg4Mg==

## CEMU 模擬器下載

https://pan.baidu.com/s/1nvhWwyP

密码: 49nf


## 遊戲緩存：

搬運自 https://www.reddit.com/r/CEMUcaches/comments/63tc83/theres_a_good_chance_that_all_12k_botw_caches/

https://pan.baidu.com/s/1qYHIa8s

下載完成解壓至 cemu\shaderCache\transferable

## GPU 設定
編輯 cemu\gameProfiles\00050000101c9500.ini

沒有此文件就新建一個

```
## TLoZ: Breath of the Wild (EUR)

[CPU]
emulateSinglePrecision = true

[Graphics]
disableGPUFence = true
```

## 手柄配置文件

SONY DS4 For Windows 驅動

https://github.com/Jays2Kings/DS4Windows/releases

XBOX 手柄插上系統會自動搜索並安裝驅動，無需手動安裝

CEMU 手柄配置文件（自選）

https://pan.baidu.com/s/1c2IMyyO

## 遊戲硬件運行說明

網友評測

https://www.youtube.com/watch?v=bd48efBLNfU

https://www.youtube.com/watch?v=AJQbHZNKjRU

FX8350/GTX 1060 3GB
The emulator only utilises less than 50% of my CPU and GPU and achieves 9-15 FPS without speedhack in open world and 25-30 FPS in the Trial.

All shrines can now be completed with the fix to magnet, stasis and cryonis

Grass and idle standing is working now as well as swimming.


##### 聲明

本站資源均來源於網絡，僅供交流學習之用，嚴禁作爲商業用途，請自覺於下載後24小時內刪除。本遊戲版權歸原作者所有，由此引發的任何版權糾紛本站概不負責。若喜歡此遊戲，請購買正版。