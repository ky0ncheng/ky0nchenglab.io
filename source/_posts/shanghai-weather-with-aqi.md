---
title: 上海天氣情況及空氣質量指數
date: 2016-12-23 17:09:59
tags:
---
[![](https://ww2.sinaimg.cn/large/6d9bd6a5gw1fbakcy1mufj20l50hbwpe.jpg)](http://sh.ky0n.xyz/map)
<div align="right"> <sup>上海空氣污染情況</sup></div>

之前一直關注的是 Twitter 上面 [美帝上海領館](https://twitter.com/CGShanghaiAir) 的空氣檢測數據

覺得不是很方便，畢竟只是一條 Tweet ，於是用了 aqicn 的 api 做了一個 [頁面](http://sh.ky0n.xyz/) 方便查看。

覺得好用的可以將 [sh.ky0n.xyz](http://sh.ky0n.xyz) 收藏爲書籤。

天氣主要數據來自 [Forecast](https://forecast.io/)

空氣質量指數來自 [Aqicn](https://aqicn.org) / [U.S. Consulate Shanghai Air Quality Monitor](http://shanghai.usembassy-china.org.cn/airmonitor.html)

災害天氣預警來自 [Thinkpage](https://www.thinkpage.cn/)

更多詳情請見：https://sh.ky0n.xyz/about


<!-- more -->