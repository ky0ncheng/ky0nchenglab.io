title: 樹莓派初始化安裝配置筆記
date: 2016-04-30 16:10:23
tags:
- raspberry
- 樹莓派
---
![](https://www.raspberrypi.org/learning/resources/lamp-web-server-with-wordpress/cover.png)

樹莓派（Raspberry Pi），是一款基於 Linux 系統的只有信用卡大小的單片機。它由英國的樹莓派基金會所開發，目的是以低價硬件及自由軟件刺激在學校的基本計算機科學教育。

2015年2月，樹莓派基金會發布了第二代產品——樹莓派2，售價35美元。樹莓派2採用4核 Broadcom BCM2836 (ARMv7-A) 芯片、雙核 VideoCore IV GPU 和 1GB 內存，其餘配置與 樹莓派B+ 型一致，除了支持第一代樹莓派支持的操作系統外，樹莓派2 將能夠運行Windows 10 IOT 以及 Snappy Ubuntu Core。

樹莓派基金會於2016年2月發布了樹莓派3,較前一代 樹莓派2，樹莓派3 的處理器升級為了 64 位元的博通 BCM2837，並首次加入了Wi-Fi無線網路及藍牙功能，而售價仍然是35美元。

---------
此文供 樹莓派 + CentOS For Arm / Raspbian Jessie 操作系統初始化安裝配置 使用。

本文完成時 樹莓派3 已經發佈，請注意系統燒寫固件版本號。

<!-- more -->

一個嶄新的樹莓派2需要自備 5V 2A 電源適配器 + MicroUSB 連接線 + 鍵盤(或者配合網線/Wifi模塊通過 SSH 遠程連接) + 鼠標(可選) + HDMI (rev 1.3 & 1.4)
連接線/VGA2HDMI 轉接顯示連接線 + 網線/(特定驅動支持的)Wifi 模塊 + TF 儲存卡 + 樹莓派保護外殼(可選)

![](https://o3ziuzht9.qnssl.com/pi-demo.jpg)

--------

* Raspbian Jessie For Raspberry Pi

先去  [Raspbian Jessie 頁面](https://www.raspberrypi.org/downloads/raspbian/) 下載系統鏡像壓縮包文件

下載完成解壓出 img 鏡像文件 使用 [Win32 Disk Imager](http://sourceforge.net/projects/win32diskimager/) 或者 [dd 命令 (for Linux / Mac OS X)](https://www.raspberrypi.org/documentation/installation/installing-images/) 燒寫系統啓動鏡像

然後燒寫 TF 儲存卡製作 Raspbian Jessie 系統啓動盤

連接完成之後連接電源啓動系統

如果使用鍵盤+鼠標(可選) 請鏈接 顯示器 完成操作。 系統默認進入 GUI 界面，若沒有鼠標請 `CTRL+ALT+F1` 切換至 CLI 界面。

或使用 [Cmder](http://cmder.net) / [日本大神修改的Putty](http://ice.hotmint.com/putty/) 連接虛擬終端

默認用戶賬戶與密碼爲：

username: pi
passwd: raspberry

若需要修改默認賬戶密碼

```bash
$ passwd
```

之後輸入兩次新密碼即可

設置 root 賬戶密碼，輸入兩次新密碼即可

```bash
$ sudo passwd root
```

啓用 root 賬戶登錄

```bash
$ sudo passwd --unlock root
```

編輯 `/etc/apt/sources.list.d/raspi.list` 替換系統默認的 `archive.raspberrypi.org` 源

```bash
# nano /etc/apt/sources.list.d/raspi.list
```

源地址內容如下

```conf
deb http://mirrors.ustc.edu.cn/archive.raspberrypi.org/debian/ jessie main ui
```

編輯 `/etc/apt/sources.list` 替換系統默認的軟件源

```bash
# nano /etc/apt/sources.list
```

源地址內容如下

```conf
deb http://mirrors.ustc.edu.cn/raspbian/raspbian/ jessie main non-free contrib rpi
deb-src http://mirrors.ustc.edu.cn/raspbian/raspbian/ jessie main non-free contrib rpi
```



更改軟件源之後 請更新系統軟件源

```bash
# apt-get update -y && apt-get upgrade -y
```

安裝常用工具

```bash
# apt-get install git psmisc wget curl dnsutils axel samba htop screen unzip zip p7zip zsh ttf-wqy-microhei ttf-wqy-zenhei xfonts-wqy -y
```

之後本人推薦 Zsh 作爲系統默認 Shell 環境

```bash
# apt-get install zsh -y
```

使用一鍵開箱的 Zsh 的配置 [oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh)

通過 curl 安裝

```bash
$ sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
```

或通過 wget 安裝

```bash
$ sh -c "$(wget https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"
```

安裝完成按提示輸入用戶密碼替換系統當前 Shell 環境 如果沒有提升請手動設置 Zsh 爲默認 Shell 環境

```bash
$ chsh -s /bin/zsh
```

到此爲止 系統基礎的設置已經完成

接下來可以裝一下自己需要的功能 例如 screen、tree、htop、cow、shadowsocks 、 chinaDNS、xunlei-fastdick 等

切記：請勿隨意斷開電源！ 關閉電源前請執行關機命令 或 重啟命令

```bash
# poweroff
```

或

```bash
# reboot
```

更多設置可以使用 `raspi-config` 命令。

 靜待系統關閉完成再拔掉電源連接線。

--------

* CentOS For ARM

CentOS 團隊的成員 Karanbir Singh 帶來了好消息，他們終於正式釋出 for ARMv7 的版本，而這個版本支援了 Raspberry Pi 2、Banana Pi 及 Cubie Truck 三片開發版。
> CentOS 7 for Arm devices like Raspberry pi2, cubie truck and bannapi is now GA: download at https://t.co/sopuycrYcy
— Karanbir Singh (@CentOS) 2015 12月 19日

下載及燒錄映像檔

先去  [CentOS 官方源](http://mirror.centos.org/altarch/7/isos/armhfp/) 下載系統鏡像壓縮包文件

比較特別的是，壓縮檔格式並不是常見的 gz、bz2、zip、tar，而是少見的 [xz](http://tukaani.org/xz/format.html)。

那要如何解壓縮呢？在 Windows 可以用 [7-Zip](http://www.developershome.com/7-zip/)，Mac 是用 [The Unarchiver](https://itunes.apple.com/us/app/the-unarchiver/id425424353?mt=12)，而 Linux 就直接用 [unxz](http://linux.die.net/man/1/unxz) 指令就可以了。

下載完成解壓出 img 鏡像文件 使用 [Win32 Disk Imager](http://sourceforge.net/projects/win32diskimager/) 或者 [dd 命令 (for Linux / Mac OS X)](https://wiki.archlinux.org/index.php/USB_flash_installation_media_(%E6%AD%A3%E9%AB%94%E4%B8%AD%E6%96%87)#.E4.BD.BF.E7.94.A8_dd) 或着適用Mac平臺的 [ApplePi-Baker](http://www.tweaking4all.com/hardware/raspberry-pi/macosx-apple-pi-baker/） 燒寫系統啓動鏡像 參考[官方說明](https://www.raspberrypi.org/documentation/installation/installing-images/)

然後燒寫 TF 儲存卡製作 CentOS For Arm 系統啓動盤

連接完成之後連接電源啓動系統

如果使用鍵盤+鼠標(可選) 請鏈接 顯示器 完成操作。 系統默認進入 GUI 界面，若沒有鼠標請 `CTRL+ALT+F1` 切換至 CLI 界面。

或使用 [Cmder](http://cmder.net) / [日本大神修改的Putty](http://ice.hotmint.com/putty/) 連接虛擬終端

username: root
passwd: centos

調整 root 分割區大小

直接依照 README （cat ~/README）裡的指令來執行。

```bash
# touch /.rootfs-repartition ; systemctl reboot
```

至 2016/04/30 為止，可用的套件數量大約是在 5100 個左右。

```bash
# yum list > packages-list.txt && wc -l packages-list.txt
```


`5189 packages-list.txt`

進到系統後的第一個動作，當然就是來更新一下套件囉。


```bash
# yum -y update
```

修改時區

那就動手把時區改成上海的時區吧，首先要找到上海時區的設定值。

```bash
# timedatectl list-timezones | grep Shanghai
```

再把時區設定成剛剛查到的設定值。

```bash
# timedatectl set-timezone 'Asia/Shanghai'
```

由於 Raspberry Pi 上面並沒有電池可以記憶時間，每次開機後都會跳回預設時間 1970-01-01 00:00。

```bash
# timedatectl
```

當然可以直接用 ntpdate 指令校時，不過會有個大問題，一但忘記執行的話，時間就會是錯誤的。

```bash
# ntpdate time.asia.apple.com
```

自動校時

加入定時計畫任務，每隔10分鐘同步系統時鐘

```bash
# crontab -e
```

`0-59/10 * * * * /usr/sbin/ntpdate us.pool.ntp.org | logger -t NTP`

或者

先安裝 NTP Server。

```bash
# yum -y install ntp
```

再啟用自動校時功能。

```bash
# timedatectl set-ntp yes
```

如果出現「Fail to set ntp: NTP not supported.」，代表我們忘了先裝好 NTP Server。

這樣子，系統就會自動校時了。

變更語系


另外，系統並沒有指定預設語系。

```bash
# localectl
```

如果我們要修改的話，先查詢一下所有支援的語系，像我習慣使用 `en_US` 美式英文 或者 繁體中文 `zh_TW` 或者 簡體中文 `zh_CN`

```bash
# localectl list-locales | grep en_US
```

再輸入系統的語系，這樣子就可以了。

```bash
# localectl set-locale LANG=en_US.utf8
```
安裝常用工具

```bash
# yum install git bind-utils psmisc wget curl samba htop unzip zip p7zip zsh -y
```

使用一鍵開箱的 Zsh 的配置 [oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh)

通過 curl 安裝

```bash
$ sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
```

或通過 wget 安裝

```bash
$ sh -c "$(wget https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"
```

安裝完成按提示輸入用戶密碼替換系統當前 Shell 環境 如果沒有提升請手動設置 Zsh 爲默認 Shell 環境

```bash
$ chsh -s /bin/zsh
```

到此爲止 系統基礎的設置已經完成

接下來可以裝一下自己需要的功能 例如 screen、tree、htop、cow、shadowsocks 、 chinaDNS、xunlei-fastdick 等

切記：請勿隨意斷開電源！ 關閉電源前請執行關機命令 或 重啟命令

```bash
# poweroff
```

或

```bash
# reboot
```

 靜待系統關閉完成再拔掉電源連接線。

推薦 DNS 由中科大提供

```
202.141.162.123
202.141.176.93
202.38.93.153
```

----------


 
 本文部分參考 [IT 技術家](http://blog.itist.tw/2016/01/install-centos-7-on-raspberry-pi-2.html)
