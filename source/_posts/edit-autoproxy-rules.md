title: Autoproxy 規則編輯說明
id: 1488

  
date: 2014-03-24 13:19:33
tags:
---

你需要這幾樣東西    notepad++   sublime text 2   和一個base64轉換器

首先把編寫好的gfvvlist.txt  轉爲base64的加密字符

![](https://ssl.daoapp.io/ww1.sinaimg.cn/large/6d9bd6a5gw1eeqrfkxoghj20r60k0djn.jpg)

把剛剛得到的base64加密字符粘貼到notepad++裏去     注意格式是UNIX換行    編碼爲UTF-8或者ANSI

![](https://ssl.daoapp.io/ww1.sinaimg.cn/large/6d9bd6a5gw1eeqrg25kphj20nx0netkq.jpg)

把第一行控制到65個字符  回車   把notepad++的窗口拉到65個字符的位置

![](https://ssl.daoapp.io/ww1.sinaimg.cn/large/6d9bd6a5gw1eeqrhws7c1j20ib0neaj6.jpg)

按下CTRL+I   分隔行

![](https://ssl.daoapp.io/ww3.sinaimg.cn/large/6d9bd6a5gw1eeqriszp4bj20ib0newo3.jpg)

複製已經分割好行數的文本粘貼到sublime       轉換爲json格式

![](https://ssl.daoapp.io/ww1.sinaimg.cn/large/6d9bd6a5gw1eeqricz7khj20lh0fmjx3.jpg)

修改文件擴展名爲.txt    大功告成

&nbsp;
