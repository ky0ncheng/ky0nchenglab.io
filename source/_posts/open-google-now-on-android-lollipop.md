title: 打開 Android 5.X Lollipop 的 Google Now 教程
date: 2015-08-16 22:25:31
tags: google
---
![](https://o3ziuzht9.qnssl.com/Google-Now.jpg)

Google Now（Google即時）是一款由Google開發的智能個人助理軟件，擴充了Google搜索手機應用程序的功能，可在Android、iOS及Google Chrome上使用。

Google Now使用自然語言用戶接口（Natural language user interface）通過一系列的Web服務來進行回答問題、提供建議、運行動作之行爲。在迴應用戶時，Google Now會基於用戶過往的的搜索習慣來預測其所可能需要的信息。Google Now最初被包含在Android 4.1 Jelly Bean（果凍豆）之中，首先支持的智能手機爲Galaxy Nexus。

美國雜誌《科技新時代》（Popular Science）將Google Now評選爲2012年的“年度創新”（Innovation of the Year）。

<div align="right"<sup>- via Wikipeidia</sup></div>

這篇文章可幫助各位打開 中文的 Google Now ~

<!--more-->

現在 Google Now 已經支持 Android、iOS和全平臺的 Chrome 瀏覽器

<video src="https://git.oschina.net/kawaiiushio/cdn/raw/master/Google%20Now%20for%20iPhone%20and%20iPad.mp4" controls="controls" width="800px">
您的瀏覽器不支持 video 標籤。
</video>
<div align="right"<sup>- Google Now For iOS 廣告</sup></div>

<video src="https://git.oschina.net/kawaiiushio/cdn/raw/master/Introducing%20Google%20Now.mp4" controls="controls" width="800px">
您的瀏覽器不支持 video 標籤。
</video>
<div align="right"<sup>- Google Now For Android 廣告</sup></div>

本教程僅支持 Android 5.x ，舊版本開啓 Google Now 方法請自行 [Google](https://google.com/ncr) 搜索

* 打開 Google Now 首先需要 [Gapps](http://wiki.cyanogenmod.org/w/Gapps) 和 [Google Search App](https://play.google.com/store/apps/details?id=com.google.android.googlequicksearchbox) 的支持

首先請拔掉手機內的 sim 卡。

系統語言需要設置英語
設置 -> 語言與輸入設定 -> 語言 -> English - (United States)

關閉手機的定位 ， 開啓飛行模式 ， 打開 Wifi ，如果在中國大陸地區，需要 <sup>*</sup>科學上網

在 Settings -> apps -> all 裡面，找到 google play service 、google service framework 、google play store 、google app ，彻底清除應用數據；

再次打開 Settings -> Accounts 選擇 Google Account，點擊 sign out，然後再次登錄 Google 賬戶

這時打開 Google Search 就可以激活 Google Now 功能了

之後可以再次開啓系統定位 關閉飛行模式 和 設置中文語言~

開啓 Google Now 可能造成 Play 商店無法正常使用 需要清除 google play service 和 google play store 的 App 數據資料

然後重新登錄 Play 商店  又可以繼續使用了

使用 Google Now 的功能 ，我推薦 [Google 即時桌面](https://play.google.com/store/apps/details?id=com.google.android.launcher) 或者 [Nova Launcher](https://play.google.com/store/apps/details?id=com.teslacoilsw.launcher)  可以帶來更好的體驗


