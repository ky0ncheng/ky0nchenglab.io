title: Foobar2000 豆瓣風格音樂播放器
id: 335

  
date: 2013-07-18 20:08:35
tags:
---

![](http://ww1.sinaimg.cn/large/6d9bd6a5jw1e6r7vlv7htj20nv0ayjth.jpg)

本軟件基於foobar 1.3 final

&nbsp;

![](http://ww4.sinaimg.cn/large/6d9bd6a5jw1e6r7yfgkujj21400ny41p.jpg)

最大化模式

![](http://ww4.sinaimg.cn/large/6d9bd6a5jw1e6r7wtvp2qj20i908nab3.jpg)

隱藏式的菜單

![](http://ww3.sinaimg.cn/large/6d9bd6a5jw1e6r84rpr53j20nl0f2tbv.jpg)

集成metro圖標

![](http://ww1.sinaimg.cn/large/6d9bd6a5gw1e731rc2lsxj20na0gajty.jpg)

![](http://ww3.sinaimg.cn/large/6d9bd6a5gw1e731sg76olj20jl0bwgn2.jpg)

歌詞插件集成

關於Foobar2000：
foobar2000 是一個 Windows 平臺下的高級音頻播放器。包含
完全支持 unicode 及支持播放增益的高級標籤功能。特色：
* 支持的音頻格式: MP3, MP4, AAC, CD Audio, WMA, Vorbis,FLAC, WavPack, WAV, AIFF, Musepack, Speex, AU, SND, 通過第三方插件將支持更多的格式。
* 完全支持 Unicode
* 輕鬆定製用戶界面佈局
* 高級標籤功能
* 可以使用轉換器組件對所有支持的格式轉換編碼格式
* 支持播放增益
* 可自定義快捷鍵
* 開放的組件體系結構允許第三方開發者來擴展播放器的功能。
* 成千上萬的自定義皮膚     [更多皮膚](http://browse.deviantart.com/customization/skins/media/foobar2000/?order=9)  本人推薦一個[metro皮膚](http://fanco86.deviantart.com/art/Metro-163447843)


皮膚 設計圖：

![](http://fc04.deviantart.net/fs70/i/2011/337/6/5/musikarte_0_4_1___eng_and_chs_by_e_r_i_c-d4h216w.jpg)
