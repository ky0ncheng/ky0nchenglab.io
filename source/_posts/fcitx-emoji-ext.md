title: "顏文字 For Fcitx QuickPhrase [beta2]"
tags:
  - Fcitx
  - Linux
id: 559

  
  
date: 2013-08-23 01:29:16
---

[![](https://ssl.daoapp.io/bcs.duapp.com/ushiopic/blog/201308/fcitx.png)](https://ssl.daoapp.io/bcs.duapp.com/ushiopic/blog/201308/fcitx.png)

把本文提供的QuickPhrase.mb放到 ~/.config/fcitx/data/QuickPhrase.mb

[![](https://ssl.daoapp.io/juanzii.me/1/wp-content/uploads/2013/08/2014-05-12-152654-的屏幕截圖11.png)](https://ssl.daoapp.io/juanzii.me/1/wp-content/uploads/2013/08/2014-05-12-152654-的屏幕截圖11.png)

&nbsp;

就可以了(沒這個文件/目錄就建一下^_^)

然後重啓fcitx輸入法 或者 註銷用戶再重新登錄

[![](https://ssl.daoapp.io/bcs.duapp.com/chromelab/blog/201307/download.png)](https://ssl.daoapp.io/pan.baidu.com/s/1mxVo6)

&nbsp;

使用方法：

首先你必須要有個Fcitx輸入法。。。廢話///

然後在中文輸入狀態 按 ;

![](https://ssl.daoapp.io/ww2.sinaimg.cn/large/6d9bd6a5gw1e7vyjl0gjwj206r01swed.jpg)

舉個例子 要打一個希臘字母 你一個打xxxl

![](https://ssl.daoapp.io/ww2.sinaimg.cn/large/6d9bd6a5gw1e7vyjwdf51j20bd023t8p.jpg)

&nbsp;

<span style="line-height: 1.714285714; font-size: 1rem;">（具體的鍵位表可以參考</span><span style="line-height: 1.714285714; font-size: 1rem;">QuickPhrase.mb 此文件可以用文字編輯器打開 </span>

![](https://ssl.daoapp.io/juanzii.me/1/wp-content/uploads/2013/08/2014-05-12-152416-的屏幕截圖11.png)

&nbsp;

本配置文件僅供測試 本人轉換於一個適用於搜狗的顏文字

我會一點點校對的  有bug可以在評論裏回覆我
