title: 日本動漫與操作系統
id: 372

  
date: 2013-07-19 21:40:50
tags:
---

今天看到一張圖，可能火星，但是很有意思。這張圖把日本動漫作品《草莓棉花糖》的 4 位主角美羽、千佳、安娜、茉莉與 4 個著名的操作系統相對應。仔細品味，感覺還很有道理。**用動漫的眼光觀察IT，似乎能夠得到不同凡響的結論。**

![](http://ww2.sinaimg.cn/large/6d9bd6a5jw1e6sg8nbqh2j20dw0bemzm.jpg)

&nbsp;

## 松岡美羽-Windows

Fun but prone to crashing（有趣但容易崩潰）

![](http://ww2.sinaimg.cn/large/6d9bd6a5jw1e6sg9ro602j202x03c0sp.jpg)<span style="line-height: 1.714285714; font-size: 1rem;"> </span>![](http://ww1.sinaimg.cn/large/6d9bd6a5jw1e6sga4mpnwj206901nglh.jpg)<span style="line-height: 1.714285714; font-size: 1rem;">    </span>

美羽是劇情中描述的活寶，不斷搞出麻煩又讓你苦笑不得的小女孩就是她了。作爲懲罰，她會經常被人打翻在地（如第一張圖）。Windows是操作系統 中最惹眼的一位，因爲用戶衆多，微軟所有的動作都會吸引全世界的眼球。從Win3.1到最新的Windows Vista，windows無論正版還是盜版，都幫助了千萬家庭走進了電腦時代乃至互聯網時代。而Windows的缺點就是經常崩潰了。從早期的藍屏到錯 誤操作，再到越來越讓人無法理解的錯誤信息。還有一點兒有趣的是，windows 7主推的觸摸操作，讓我聯想到動畫中鬆崗美羽在自己腦門畫了一個“發射”按鈕，讓她的朋友們按，所以現在看windows 7的宣傳視頻，我總是忍不住笑場。

## 伊藤千佳-Linux

Reliable but boring（可靠但有點兒無聊）

![](http://ww2.sinaimg.cn/large/6d9bd6a5jw1e6sgc8henij202x03c0so.jpg) ![](http://ww4.sinaimg.cn/large/6d9bd6a5jw1e6sgci7ktyj2046050q2v.jpg)

千佳是個乖孩子，好好學習，好好與大家相處，好好玩。莫非是傳說中的真·三好學生？！千佳是其他三人的好朋友，但在美羽那誇張表演的掩蓋下，觀衆經 常會將她遺忘。Linux似乎也是這樣，Windows在個人電腦平臺的壟斷地位導致初級用戶根本不知道自己還有另外一個選擇。實際上Linux已經在服 務器市場默默地耕耘了多年。而個人用戶似乎也對linux下的命令行、編譯、安裝等感到頭痛。Linux是無聊的程序員自己玩樂的工具，這樣的觀點成爲了 Windows用戶們的共識。但現在好了，以ubuntu、fedora、openSUSE等發行版爲代表的linux面向個人用戶，雙擊安裝、 GNOME與KDE可以和vista或OS X的UI相媲美，Linux在個人用戶版本上，正在向更方便、更簡單的人性化發展。面對[windows黑屏](http://www.google.com/search?q=windows%E9%BB%91%E5%B1%8F&amp;ie=utf-8&amp;oe=utf-8&amp;aq=t&amp;rls=org.mozilla:zh-CN:official&amp;client=firefox-a)，我們多了一種選擇。

&nbsp;

## 安娜·科波拉-OS X

Looks nice but is useless（華而不實）

![](http://ww2.sinaimg.cn/large/6d9bd6a5jw1e6sgd3i0zqj202x03ct8n.jpg) ![](http://ww3.sinaimg.cn/large/6d9bd6a5jw1e6sgdecktqj201o020742.jpg)

安娜是漂亮的英國女孩，金髮碧眼的她最受男孩子的歡迎，但是，實際上安娜是完全日化的：她的英語比日本人的日語還差，喜歡吃米飯剩過吃麪包。漂亮的 外表下，安娜是很愛面子的，把喜歡的日文CD藏起來放上古典音樂，私下裏找茉莉學英文，但她實在是太可愛了，讓人對她討厭不起來。蘋果的OS X因爲出色的UI被fans奉爲藝術品，從Windows支持主題那一天起，各大主題站點下載量最高的就是把windows弄成OS X的主題。但蘋果的很多操作習慣與windows用戶的偏好有很大的區別，比如說觸摸版單鍵還是雙鍵的問題，這成爲了很多windows用戶嘲笑Mac的 把柄。但無法否認的是，漂亮是一種優勢。Mac的fans會爲之瘋狂。不過，能否把iPhone的複製、粘貼這個小功能賜給你的用戶呢？這就要看Jobs與他的團隊對於“藝術”的看法了，沒準是“彪悍的iPhone不需要C/P”。

&nbsp;

## 櫻木茉莉-FreeBSD

Meek and goes unnoticed（順從而容易被遺忘）

![](http://ww4.sinaimg.cn/large/6d9bd6a5jw1e6sgfpyus2j202x03ct8o.jpg) ![](http://ww2.sinaimg.cn/large/6d9bd6a5jw1e6sgfynpnej206y02i3yf.jpg)

茉莉是膽小而敏感的小姑娘，要不是發現安娜不會英語的真相，就要成爲衆人欺負的對象了。其實茉莉善良、聰明、天真，是四個孩子裏最像普通小學生的。 FreeBSD作爲加州大學伯克利分校內走出的操作系統，它保留了UNIX的純正性，同時還透着學生氣。這個操作系統更像是爲方便教授《操作系統》這門課 程，而不是面向個人用戶開發的。看看那冗長的安裝說明，你就知道，這枯燥乏味而富有挑戰性的操作系統，是多麼適合教授給學生講解操作系統種種複雜而優秀的機制的。當然，FreeBSD作爲服務器操作系統來說是一個非常不錯的選擇，但對於個人用戶，這個學生還有很長的路要走。

對了，是不是少了什麼，最後一個主角，伸惠姐姐對應是什麼？

&nbsp;

## 伊藤伸惠-OS/360

（？）：？

![](http://ww2.sinaimg.cn/large/6d9bd6a5jw1e6sggy6zp4j206o050q32.jpg)

伸惠姐姐是千佳的姐姐，短期大學生（相當於我國的大專生），待業，愛抽菸。把OS/2放在這裏，其實是想說OS/360如伸惠姐姐一般，是4個孩子 的中心。由IBM與196x開發的這個操作系統，現在似乎只能在博物館看到了。但是他對未來的操作系統影響巨大。當時的科學家們無論如何也不會想到半個世 紀之後的人會用電腦上網、看電影、玩遊戲、發帖評論他們吧。但是，如果當時沒有他們的開拓，我們現在可能還只能在（非中國的）大學中見到像立櫃一樣，稱作 電腦的東西。

而現在，我們可以享受OS給我們提供的便利，我們可以期待Windows 7，ubuntu、Fedora、openSUSE的新版本，Mac OS X Snow Leopard，FreeBSD 7.2等等。

&nbsp;

本文轉自  [http://blog.jobbole.com/43681/](http://blog.jobbole.com/43681/)

&nbsp;
