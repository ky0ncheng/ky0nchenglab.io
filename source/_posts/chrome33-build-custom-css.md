title: Chrome 33+ 自建 擴展 實現 custom.css
tags:
  - Chrome 
date: 2014-02-21 18:27:26
---

實現步驟：
1、新建一個文件夾，比如customCSS，包含custom.css和manifest.json，custom.css就是新版被閹割的那個文件
2、manifest.json的內容如下
<pre class="brush: js; title: JavaScript">{
   "content_scripts": [ {
      "css": ["custom.css"],
      "matches": [ "http://*/*", "https://*/*" ]
   } ],
   "description": "custom.css",
   "name": "custom.css",
   "version": "1.0",
   "manifest_version": 2
}</pre>
description是擴展簡介，name是擴展名字，version是版本號，都可以自己改
3、打開chrome的擴展管理，選擇打包擴展程序，擴展程序根目錄選擇customCSS文件夾，私有密匙文件不用管，然後選擇打包擴展程序，生成customCSS.crx和customCSS.pem密匙
4、將customCSS.crx拖入擴展管理安裝即可，任務管理器裏面無進程

擴展模板下載 [http://pan.baidu.com/s/1c0zO3G8](http://pan.baidu.com/s/1c0zO3G8)

以後還要修改可以去userdata裏找到擴展文件夾 修改css文件即可

分享我的custom.css一個
<pre class="brush: css; title: Css">* {text-decoration:none!important;}
::-webkit-scrollbar{
width:3px;
height:6px;
background-color:#fff;
}
::-webkit-scrollbar:hover{
background-color:#eee;
}
::-webkit-scrollbar-thumb{
min-height:5px;
min-width:5px;
-webkit-border-radius:20px;border:1px solid#888;
::-webkit-border-radius:1px;
background-color: #AAA;
}
::-webkit-scrollbar-thumb:hover{
min-height:4px;
min-width:4px;
-webkit-border-radius:20px;border:1px solid#444;
background-color: #AAA;
}
::-webkit-scrollbar-thumb:active{
-webkit-border-radius:20px;border:1px solid#444;
background-color: #AAA;
}</pre>
