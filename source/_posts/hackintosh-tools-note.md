---
title: 黑蘋果相關工具筆記
date: 2016-11-30 17:09:59
tags:
- hackintosh
- mac
---
![](https://o3ziuzht9.qnssl.com/hackintosh-logo.jpg)

這篇文章只是一個關於黑蘋果安裝完成之後的小工具使用筆記，具體按照步驟見：[Tonymacx86](https://www.tonymacx86.com/threads/unibeast-install-macos-sierra-on-any-supported-intel-based-pc.200564/)

<!-- more -->

Clover https://sourceforge.net/projects/cloverefiboot/

Clover Configurator http://mackie100projects.altervista.org/download-clover-configurator/

FakeSMC https://bitbucket.org/RehabMan/os-x-fakesmc-kozlek/downloads

Kext Utility http://cvad-mac.narod.ru/index/0-4

KCPM Utility Pro https://www.firewolf.science/2016/09/kcpm-utility-pro-v6-brand-new-kexts-ezinstaller-macos-sierra-supported-repairing-permissions-configuring-rootless-and-more/

Nvidia Web Driver http://www.insanelymac.com/forum/topic/312525-nvidia-web-driver-updates-for-macos-sierra-update-09202016/

LAN & Wireless Driver http://www.insanelymac.com/forum/files/category/5-lan-and-wireless/
