title: FBI Warning 樣式的 SSH 登錄提示
date: 2015-10-19 16:10:23
tags:
---
在微博上看到秋大的 SSH 登錄提示 覺得很有意思

其實我什麼都不知道 深藏功與名

![](https://o3ziuzht9.qnssl.com/6529f26ajw1ex66moq1x5j21kw0vywl8.jpg)

我嘗試著修改 /etc/motd 發現並不支持顏色顯示 噗。。。

然後微博上的小夥伴告訴我這個是修改 shell 的 rc 文件，每次啟動 terminal 的時候執行 sh 腳本就可以了

<!-- more -->

代碼參考自 http://blog.depressedmarvin.com/2015/10/13/fbi-warning-ssh-banner/

修改了最後的居中文本




1. 打開 https://gist.github.com/ky0ncheng/aa936fe51acd064ea25e 將裡面的代碼複製或另存為 xxx.sh

2. 保存完成後 chmod +x xxx.sh

3. 修改 shell 的 rc 文件最後加入 bash xxx.sh

以至於啟動 terminal 後執行 shell 腳本

  * bash shell 修改 /etc/bash.bashrc
  * zsh 修改 ~/.zshrc


效果如圖

![](https://o3ziuzht9.qnssl.com/fbi-ssh-banner-pic.png)
