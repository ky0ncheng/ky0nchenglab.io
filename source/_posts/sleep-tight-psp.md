title: Sleep Tight, PSP.
date: 2016-03-01 21:02:36
tags: PSP SONY
---
![](https://o3ziuzht9.qnssl.com/psp.png)

看到 [Cnbeta](http://www.cnbeta.com/articles/479461.htm) 的新聞，我也打算拿起我的PSP給它重生到最新的系統。

PSP從二零零四年十二月十二日出世，已經度過了十二年共七次硬件更新，實在是索尼大法的良心作。

<!-- more -->
如果要升級到最新的6.61系統，可以嘗試使用把系統鏡像放到儲存卡內進行更新作業。詳見 [SONY 支援](https://asia.playstation.com/tw/cht/support/sysupdate?id=-99&platformId=4)

2015.01.15日的6.61更新日誌：（等於沒寫

        6.61版本更新的主要機能
        已改善了使用部分機能時系統軟件的穩定性。
        
 ---------

[6.61 SONY 官方鏡像下載]()


開發者Wololo 的系統完美破解 [下載](http://wololo.net/talk/viewtopic.php?f=17&t=41249)

反正大法要關PSP的PSN了，未來只能玩UMD或者盜版的遊戲了。在大陸也很難買到正版的PSP遊戲，你懂的。

自己整理的一些文件分享給大家 [Mega網盤](https://mega.nz/#F!8hpXFbbD!8ylbuJrFyWYNm5k4MAnoYA)

最後感謝PSP給我打發了很多的無聊時間。

![](https://o3ziuzht9.qnssl.com/photo_2016-03-01_21-33-54.jpg)

Sleep Tight, PSP.
