title: 探訪 谷歌上海辦公室
tags:
- Google
- Shanghai
id: 251


date: 2013-07-12 15:10:28
---

![](http://ww1.sinaimg.cn/large/6d9bd6a5jw1e6k0n9vx1nj20ur0j5q6u.jpg)

![](http://ww4.sinaimg.cn/large/6d9bd6a5jw1e6k0ol2j9lj207g03xmxb.jpg)

2012年4月25日  Google 上海正式搬入了環球金融中心，新辦公室的設計充滿了中國元素的風情。我們一起來看看工程師們的聖地。

&nbsp;

&nbsp;

![](http://ww4.sinaimg.cn/large/6d9bd6a5jw1e6k0r2bwwej20ca0fcdhs.jpg)

山水屏幕——揚子江上獨釣老翁

![](http://ww1.sinaimg.cn/large/6d9bd6a5jw1e6k0rnv8i9j20ca0fcjt3.jpg)

![](http://ww4.sinaimg.cn/large/6d9bd6a5jw1e6k0s5vmqej20br0g0go4.jpg)

福利——茶歇水果，應有盡有

![](http://static.36kr.com/wp-content/uploads/2012/04/canting.jpg)

上面的文章來自谷歌搬到新家的報道

接下來是今天我自己拍的   我也是匆忙收到谷歌的一個電話  讓我去參觀的  _(:з」∠)_

![](http://ww4.sinaimg.cn/mw690/6d9bd6a5jw1e6k0g4pxfgj20p00e277t.jpg)

這是我的Pass    那個藍色的圓形帖紙是進入餐廳要用的

![](http://ww1.sinaimg.cn/mw690/6d9bd6a5jw1e6k0fn0ma5j20p00e2jte.jpg)

由於谷歌幾年前被迫離開中國  在大陸的業務就剩下廣告了 _(:з」∠)_

![](http://ww1.sinaimg.cn/mw690/6d9bd6a5jw1e6k0cw3utej20p00e2djp.jpg)

這是入口處

![](http://ww2.sinaimg.cn/mw690/6d9bd6a5jw1e6k0d1oszsj20p00e2td1.jpg)

![](http://ww3.sinaimg.cn/mw690/6d9bd6a5jw1e6k0d5gvhvj20p00e2427.jpg)

路過谷歌的餐廳  等會要來這裏吃飯的 ^_^

![](http://ww3.sinaimg.cn/mw690/6d9bd6a5jw1e6k0d91b1kj20p00e2q5e.jpg)

谷歌內置的咖啡廳  名字叫：海 Cafe

![](http://ww1.sinaimg.cn/mw690/6d9bd6a5jw1e6k0daxvyxj20p00e2jud.jpg)

![](http://ww2.sinaimg.cn/mw690/6d9bd6a5jw1e6k0eo5rruj20p00e2dip.jpg)

我朝小朋友給Google畫的Doodle~  很可愛啊~ 很童趣

![](http://ww2.sinaimg.cn/mw690/6d9bd6a5jw1e6k0dcrf8aj20p00e2n0h.jpg)

![](http://ww2.sinaimg.cn/mw690/6d9bd6a5jw1e6k0e4k02hj20p00e2ado.jpg)

![](http://ww2.sinaimg.cn/mw690/6d9bd6a5jw1e6k0e9nqntj20p00e2goh.jpg)

![](http://ww3.sinaimg.cn/mw690/6d9bd6a5jw1e6k1f0rbb9j20ow0dswi6.jpg)

地板上的字

![](http://ww4.sinaimg.cn/mw690/6d9bd6a5jw1e6k0dg3t9aj20p00e2q5z.jpg)

![](http://ww1.sinaimg.cn/mw690/6d9bd6a5jw1e6k0e2js29j20p00e20w2.jpg)

谷歌的前臺   （妹子很漂亮哦~

![](http://ww1.sinaimg.cn/mw690/6d9bd6a5jw1e6k0dkwr3jj20p00e2413.jpg)

[![](http://ww3.sinaimg.cn/mw690/6d9bd6a5jw1e6k0dtrtr6j20p00e2dis.jpg)](http://ww3.sinaimg.cn/mw690/6d9bd6a5jw1e6k0dtrtr6j20p00e2dis.jpg)

入口處有個很寬的落地窗 放眼望去 是整個陸家嘴的鳥瞰

![](http://ww2.sinaimg.cn/mw690/6d9bd6a5jw1e6k0dwxki5j20p00e2411.jpg)

旁邊的我朝特色的大門

&nbsp;

![](http://ww1.sinaimg.cn/mw690/6d9bd6a5jw1e6k0dorojuj20p00e2djc.jpg)

Chrome和安德猴的雕塑

![](http://ww4.sinaimg.cn/mw690/6d9bd6a5jw1e6k0dqbkm6j20p00e2die.jpg)

![](http://ww1.sinaimg.cn/mw690/6d9bd6a5jw1e6k0dykb6ij20p00e2mzz.jpg)

入口還放了一個谷歌 地球的 設備  可以看全立體的地圖影像

&nbsp;

&nbsp;

&nbsp;

![](http://ww2.sinaimg.cn/mw690/6d9bd6a5jw1e6k0djm4q3j20p00e2goh.jpg)

每個辦公室都有一個名字  這個叫做：黃河

由於辦公區域不能拍照  只能口述了

谷歌在環球金融中心有兩個樓層  61和62   單單辦公室 佔了一個樓層多

辦公室裏 可以騎車  可以遛狗  看到有人在辦公室裏養了只狗。。。

![](http://ww2.sinaimg.cn/large/6d9bd6a5jw1e6k1lgcchaj20e20own0f.jpg)

辦公室的音樂---留聲機

每個辦公區域  都有個中文名稱 我看到的有  仁 義 智  信 孝    （沒見到 “禮”    出自《仁義八行》吧？ 仁 智 義 忠 信 禮 孝 悌

谷歌職員每個人的電腦屏幕大概有27-30寸   也有一個人有幾個屏幕的  系統清一色的ubuntu。。。。壯哉我大烏班圖  也有Mac  很少看見Windows    So Good！（乾死微軟先

&nbsp;

![](http://ww3.sinaimg.cn/mw690/6d9bd6a5jw1e6k0emn1vbj20p00e20wb.jpg)

荷塘月色

我應該要去吃飯了

![](http://ww2.sinaimg.cn/mw690/6d9bd6a5jw1e6k0et8norj20p00e20ur.jpg)

餐廳牆上貼的紙頭

![](http://ww3.sinaimg.cn/mw690/6d9bd6a5jw1e6k0ewpe1gj20p00e20vk.jpg)

三爽相機的變焦   說好還是不好呢 ？  _(:з」∠)_

![](http://ww2.sinaimg.cn/mw690/6d9bd6a5jw1e6k0eym2v3j20p00e2jvb.jpg)

![](http://ww2.sinaimg.cn/large/6d9bd6a5jw1e6k1n1532lj20ao0j4q4f.jpg)

![](http://ww1.sinaimg.cn/mw690/6d9bd6a5jw1e6k0feulzwj20p00e2jv3.jpg)

餐廳人還是很多的

![](http://ww2.sinaimg.cn/large/6d9bd6a5jw1e6k1qjobl1j20ao0j4myy.jpg)

![](http://ww3.sinaimg.cn/mw690/6d9bd6a5jw1e6k0f5sf9rj20p00e2dkq.jpg)

![](http://ww4.sinaimg.cn/mw690/6d9bd6a5jw1e6k0fyuntnj20p00e279c.jpg)

從餐廳的落地窗放眼望去  大樓矗立

![](http://ww2.sinaimg.cn/mw690/6d9bd6a5jw1e6k0fi98hzj20p00e2q59.jpg)

掛的鍾

裏面的菜和飲料都是Free的  自助式 隨便拿

我拿了一片Pizza 兩個蛋撻  一個麪包  已被谷歌特製的奶茶  又取了一些餐後水果  哈密瓜和西瓜的拼盤

&nbsp;

我吃完 聊(gao)了一會天（gay）就回去了      (＾Ｕ＾)ノ~ＹＯ

![](http://ww2.sinaimg.cn/mw690/6d9bd6a5jw1e6k0fk0s0cj20p00e2juo.jpg)

谷歌的辦公室有兩層  所以有樓梯

![](http://ww3.sinaimg.cn/mw690/6d9bd6a5jw1e6k0fpm0unj20p00e2jth.jpg)

61層

![](http://ww4.sinaimg.cn/mw690/6d9bd6a5jw1e6k0ft3rgmj20p00e2ad1.jpg)

GDG  另一件Tee  我有一件白色個GDG和一件黑色的I/O

![](http://ww3.sinaimg.cn/mw690/6d9bd6a5jw1e6k0g0oqkhj20p00e2djj.jpg)

回家咯~
