---
title: Let's benchmark
date: 2016-10-06 17:09:59
tags:
---
![](https://o3ziuzht9.qnssl.com/benchmark.jpg)

本文章將提供一些主流系統平臺的跑分軟件。

<!-- more -->

<h1>Windows<h1>

CPU & GPU 信息+跑分

[CineBench](https://www.maxon.net/en/products/cinebench/)

[CPU-Z](http://www.cpuid.com/softwares/cpu-z.html)

![](https://ssl.daoapp.io/tpucdn.com/gpuz/screen1-v2.gif)

[GPU-Z](https://www.techpowerup.com/gpuz/)

![](https://ssl.daoapp.io/crystalmark.info/software/CrystalCPUID/images/CrystalCPUID4_10.png)

[CrystalCPUID](http://crystalmark.info/software/CrystalCPUID/index-e.html)


SSD 測速跑分

![](https://ssl.daoapp.io/alex-is.de/temp/as-ssd-bench.png)

[AS SSD Benchmark](http://www.alex-is.de/PHP/fusion/downloads.php?cat_id=4&download_id=9)

![](https://ssl.daoapp.io/suishoshizuku.com/images/CrystalDiskInfo6-en.jpg)

[CrystalDiskInfo 6](http://suishoshizuku.com/en/software/)


HDD 檢測

![](https://ssl.daoapp.io/www.hdtune.com/images/screenshot.png)

[HD Tune Pro](http://www.hdtune.com/)

附：資料

CPU 天梯圖 [English](http://cpubenchmark.net/high_end_cpus.html) [中文](http://www.mydrivers.com/zhuanti/tianti/cpu/)

GPU 天梯圖 [English](http://www.videocardbenchmark.net/high_end_gpus.html) [中文](http://www.mydrivers.com/zhuanti/tianti/gpu/)

筆記本 CPU 天梯圖 [中文](http://www.mydrivers.com/zhuanti/tianti/cpum/index.html)

筆記本 GPU 天梯圖 [中文](http://www.mydrivers.com/zhuanti/tianti/gpum/index.html)


<h1>Android & iOS<h1>

GeekBench4

![](https://o3ziuzht9.qnssl.com/geekbench4.jpg)

[![](https://o3ziuzht9.qnssl.com/android_badge.png)](https://play.google.com/store/apps/details?id=com.primatelabs.geekbench)

![https://itunes.apple.com/us/app/geekbench-4/id1130770356?mt=8](https://o3ziuzht9.qnssl.com/apple_badge.png)

AIDA64

![](https://o3ziuzht9.qnssl.com/aida64.jpg)

[![](https://o3ziuzht9.qnssl.com/android_badge.png)](https://play.google.com/store/apps/details?id=com.finalwire.aida64)

CPU-Z

![](https://o3ziuzht9.qnssl.com/cpuz.jpg)

[![](https://o3ziuzht9.qnssl.com/android_badge.png)](https://play.google.com/store/apps/details?id=com.cpuid.cpu_z)

Wifi 掃描儀

![](https://o3ziuzht9.qnssl.com/wifi_ana.jpg)

[![](https://o3ziuzht9.qnssl.com/android_badge.png)](https://play.google.com/store/apps/details?id=com.farproc.wifi.analyzer)

GPS測試

[![](https://o3ziuzht9.qnssl.com/android_badge.png)](https://play.google.com/store/apps/details?id=cache.wind.gps)

網絡信號信息

[![](https://o3ziuzht9.qnssl.com/android_badge.png)](https://play.google.com/store/apps/details?id=de.android.telnet)

附：資料

移動平臺 CPU 天梯圖 [中文](http://www.mydrivers.com/zhuanti/tianti/01/index.html)

# Linux

硬件檢測

```bash
wget --no-check-certificate https://github.com/teddysun/across/raw/master/unixbench.sh
chmod +x unixbench.sh
./unixbench.sh
```

測試項目：
Dhrystone 2 using register variables
此項用於測試 string handling，因爲沒有浮點操作，所以深受軟件和硬件設計（hardware and software design）、編譯和鏈接（compiler and linker options）、代碼優化（code optimazaton）、對內存的cache（cache memory）、等待狀態（wait states）、整數數據類型（integer data types）的影響。

Double-Precision Whetstone
這一項測試浮點數操作的速度和效率。這一測試包括幾個模塊，每個模塊都包括一組用於科學計算的操作。覆蓋面很廣的一系列 c 函數：sin，cos，sqrt，exp，log 被用於整數和浮點數的數學運算、數組訪問、條件分支（conditional branch）和程序調用。此測試同時測試了整數和浮點數算術運算。

Execl Throughput
此測試考察每秒鐘可以執行的 execl 系統調用的次數。 execl 系統調用是 exec 函數族的一員。它和其他一些與之相似的命令一樣是 execve（） 函數的前端。

File copy
測試從一個文件向另外一個文件傳輸數據的速率。每次測試使用不同大小的緩衝區。這一針對文件 read、write、copy 操作的測試統計規定時間（默認是 10s）內的文件 read、write、copy 操作次數。

Pipe Throughput
管道（pipe）是進程間交流的最簡單方式，這裏的 Pipe throughtput 指的是一秒鐘內一個進程可以向一個管道寫 512 字節數據然後再讀回的次數。需要注意的是，pipe throughtput 在實際編程中沒有對應的真實存在。

Pipe-based Context Switching
這個測試兩個進程（每秒鐘）通過一個管道交換一個不斷增長的整數的次數。這一點很向現實編程中的一些應用，這個測試程序首先創建一個子進程，再和這個子進程進行雙向的管道傳輸。

Process Creation
測試每秒鐘一個進程可以創建子進程然後收回子進程的次數（子進程一定立即退出）。process creation 的關注點是新進程進程控制塊（process control block）的創建和內存分配，即一針見血地關注內存帶寬。一般說來，這個測試被用於對操作系統進程創建這一系統調用的不同實現的比較。

System Call Overhead
測試進入和離開操作系統內核的代價，即一次系統調用的代價。它利用一個反覆地調用 getpid 函數的小程序達到此目的。

Shell Scripts
測試一秒鐘內一個進程可以併發地開始一個 shell 腳本的 n 個拷貝的次數，n 一般取值 1，2，4，8。（我在測試時取 1， 8）。這個腳本對一個數據文件進行一系列的變形操作（transformation）。


網絡檢測

```bash
wget --no-check-certificate https://github.com/teddysun/across/raw/master/bench.sh
chmod +x bench.sh
./bench.sh
```

![](https://ssl.daoapp.io/teddysun.com/wp-content/uploads/2015/speedtest_bwg.png)

本文參考：

https://teddysun.com/245.html

https://teddysun.com/444.html