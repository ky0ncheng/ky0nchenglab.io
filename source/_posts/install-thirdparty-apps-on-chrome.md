title: 允許Chrome安裝第三方網站的腳本以及擴展的方法
id: 1844

  
date: 2014-07-01 19:09:19
tags:
---

涵蓋 Windows XP/7 、Mac OS X 、Linux 的 Chrome 第三方應用安裝策略說明。

聽說衆多 Chrome 粉絲爲 Google 禁止安裝第三方應用的問題感到相當煩惱。那麼我就來

簡單地解救一下。

實現的方法,其實只是利用了 Chrome 爲企業批量配置 Chrome 瀏覽器提供的策略配置功能罷了,這是 N 年前就有的功能了,只是方法麻煩了一點,大家沒了解過。

Windows XP 在進行配置前得先安裝羣組策略管理工具(GPMC),以及該工具所須的組件:

1\. Windows 圖像處理組件(WIC)  [http://www.microsoft.com/zh-cn/download/details.aspx?id=32](http://www.microsoft.com/zh-cn/download/details.aspx?id=32)

2\. Microsoft .NET Framework 1.1  [http://www.microsoft.com/zh-cn/download/details.aspx?id=26](http://www.microsoft.com/zh-cn/download/details.aspx?id=26)

3\. Group Policy Management Console (GPMC) with Service Pack 1  [http://www.microsoft.com/zh-CN/download/details.aspx?id=21895](http://www.microsoft.com/zh-CN/download/details.aspx?id=21895)

然後,我沒有 Windows 7 可供測試(求支援),我只是在用 XP ,所以我有點標題黨了......

先下載一個官方提供的壓縮包並解壓縮:[http://dl.google.com/dl/edgedl/chrome/poli](http://dl.google.com/dl/edgedl/chrome/policy/policy_templates.zip)[cy/policy_templates.zip](http://dl.google.com/dl/edgedl/chrome/policy/policy_templates.zip)

按 Ctrl­R 調出運行窗口,輸入 gpedit.msc並運行之:

1\. 選擇左側窗口的「計算機配置」——「管理模板」,鼠標右鍵單擊,選擇「添加/刪除模板」。

2\. 按左下角「添加」按鈕添加剛纔解壓出來的文件夾的 windowsadmzh-­CNchrome.adm文件

3\. 左側出現了「計算機配置——管理模板(——「經典管理模板(ADM)」)——Google——Google Chrome」的目錄。

4\. 進入子目錄「擴展程序」,雙擊「配置擴展程序、應用和用戶腳本安裝源」開始修改配置。

5\. 在出現的窗口中選擇「已啓用」,然後點擊「顯示」。

6\. 在新出現的窗口中點擊「添加」,輸入 *://*/*,確定。

7\. 繼續按「確定」確認修改並關閉窗口,再按剩下窗口右下的「應用」。

最後打開 Chrome 的 chrome://policy 頁面,點擊左上方的按鈕重新加載策略信息便可令設置即時生效。

&nbsp;

沒有圖片總是有點不太好,可以參考這篇官方文章:[https://support.google.com/installer/answer/146164?hl=en](https://support.google.com/installer/answer/146164?hl=en)

中文版(沒配圖):[https://support.google.com/installer/answer/146164?hl=zh-Hans](https://support.google.com/installer/answer/146164?hl=zh-Hans)

不通過 chrome://plugins 頁面停用 update 插件來取消自動更新的方法也在裏面。

Mac OS X

測試環境:Mac OS X 10.9.2

運行 Terminal ,執行以下命令:

# 設定允許提供腳本和擴展的網站
<pre class="lang:default decode:true">defaults write com.google.Chrome ExtensionInstallSources -array "*://*/*"</pre>
# 還原方法
<pre class="lang:default decode:true">defaults delete com.google.Chrome ExtensionInstallSources</pre>
然後打開 Chrome 的 chrome://policy 頁面,點擊左上方的按鈕重新加載策略信息便可令設置即時生效。

&nbsp;

Linux

先用文本編輯工具編寫文件 custom_policies.json,名字不重要,文件後綴名爲 .json即可:
<pre class="lang:default decode:true">{

  "ExtensionInstallSources": [
    "*://*/*"
  ]
}</pre>
接着運行 Terminal ,執行以下命令,最終效果和上面 Mac 的說明一樣:

# 創建官方指定的特殊文件夾,用於存放策略文件。
<pre class="lang:default decode:true ">sudo mkdir -­p /etc/opt/chrome/policies/recommended</pre>
# 移動剛剛創建的 custom_policies.json 到上面創建的文件夾
<pre class="lang:default decode:true">sudo mv custom_policies.json /etc/opt/chrome/policies/recommended/</pre>
最後打開 Chrome 的 chrome://policy 頁面,點擊左上方的按鈕重新加載策略信息便可令設置即時生效。

[參考資料]Exhausitive List of All Manageable Policies [http://www.chromium.org/administrators/policy-list-3](http://www.chromium.org/administrators/policy-list-3)

Policy Template for Windows &lt;http://www.chromium.org/administrators/policy-templates&gt;

[http://www.chromium.org/administrators/mac-quick-start](http://www.chromium.org/administrators/mac-quick-start)

[http://www.chromium.org/administrators/linux-quick-start](http://www.chromium.org/administrators/linux-quick-start)

&nbsp;

原文轉載自 [鉛筆的博客](https://drive.google.com/folderview?id=0By-rJN-esrnrWVc2ZWJhZlJ2cDA&amp;usp=sharing)
