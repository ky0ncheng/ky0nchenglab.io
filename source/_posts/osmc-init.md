title: OSMC 配置筆記
date: 2016-07-18 16:10:23
tags:
- osmc
- debian
- 樹莓派
---
![](https://www.raspberrypi.org/learning/resources/lamp-web-server-with-wordpress/cover.png)

樹莓派（Raspberry Pi），是一款基於 Linux 系統的只有信用卡大小的單片機。它由英國的樹莓派基金會所開發，目的是以低價硬件及自由軟件刺激在學校的基本計算機科學教育。

2015年2月，樹莓派基金會發布了第二代產品——樹莓派2，售價 35 美元。樹莓派2 採用 4 核 Broadcom BCM2836 (ARMv7-A) 芯片、雙核 VideoCore IV GPU 和 1GB 內存，其餘配置與 樹莓派B+ 型一致，除了支持第一代樹莓派支持的操作系統外，樹莓派2 將能夠運行Windows 10 IOT 以及 Snappy Ubuntu Core。

樹莓派基金會於 2016年 2月 發布了樹莓派3,較前一代樹莓派2，樹莓派3 的處理器升級為了 64 位元的博通 BCM2837，並首次加入了 Wi-Fi 無線網路及藍牙功能，而售價仍然是 35 美元。

---------

![](https://osmc.tv/assets/img/osmc_meta.png?v=4b2bdd20f2)

此文供 樹莓派 + OSMC (with Debian Jessie) 操作系統初始化安裝配置 使用。

<!-- more -->

一個嶄新的 樹莓派2 需要自備 5V 2A 電源適配器 + MicroUSB 連接線 + 鍵盤(或者配合網線/Wifi模塊 通過 SSH 遠程連接) + 鼠標(可選) + HDMI (rev 1.3 & 1.4)
線連接顯示器 + 網線/內置Wifi模塊(Raspberry Pi 3)/某些支持默認驅動的無線網卡 + TF 儲存卡 + 樹莓派保護外殼(可選)

![](https://o3ziuzht9.qnssl.com/pi-demo.jpg)


* 下載及燒錄映像檔

先去  [OSMC 官方網站](https://osmc.tv/download/) 下載系統鏡像壓縮包文件

下載完成解壓出 img 鏡像文件 使用 [Win32 Disk Imager](http://sourceforge.net/projects/win32diskimager/) 或者 [dd 命令 (for Linux / Mac OS X)](https://wiki.archlinux.org/index.php/USB_flash_installation_media_(%E6%AD%A3%E9%AB%94%E4%B8%AD%E6%96%87)#.E4.BD.BF.E7.94.A8_dd) 或着適用Mac平臺的 [ApplePi-Baker](http://www.tweaking4all.com/hardware/raspberry-pi/macosx-apple-pi-baker/） 燒寫系統啓動鏡像 參考[官方說明](https://www.raspberrypi.org/documentation/installation/installing-images/)

然後燒寫 TF 儲存卡製作 OSMC 系統啓動盤

* 配置系統

連接完成之後連接電源啓動系統

推薦使用鍵盤+鼠標(可選) 請鏈接 顯示器 完成操作。 系統默認進入 GUI 界面。


首次開機會自動調整 root 或儲存卡分割區大小，如下圖所示。

![](https://o3ziuzht9.qnssl.com/gparted_screenshot.png)


可以通過 GUI 介面直接設置時區，系統字符編碼等（簡繁中文可以由於字體問題顯示「豆腐方塊」），推薦設置為 English 。

OMSC 支持很多插件，可以直接在 GUI 介面內安裝。

如果使用 smb 協議連載自己的遠程文件夾，可以開放 smb 的匿名用戶登錄功能，這樣可以直接使用 OSMC 連接媒體庫。

以下是 CLI 配置

----------------
* SSH 連接系統

Windows 系統可以使用 [Cmder](http://cmder.net) / [日本大神修改的Putty](http://ice.hotmint.com/putty/) 通過 SSH 連接虛擬終端

username: osmc
passwd: osmc

若需要修改默認賬戶密碼

```bash
$ passwd
```

之後輸入兩次新密碼即可

設置 root 賬戶密碼，輸入兩次新密碼即可

```bash
$ sudo passwd root
```

啓用 root 賬戶登錄

```bash
$ sudo passwd --unlock root
```

* 安裝常用工具

```bash
# apt-get install git bind-utils apt-transport-https psmisc tree wget curl samba htop unzip zip p7zip zsh -y
```

* 更新系統軟件源

```bash
# apt-get update -y && apt-get upgrade -y
```
編輯 `/etc/apt/sources.list` 替換系統默認的軟件源

```bash
# nano /etc/apt/sources.list
```

源地址內容如下

```conf
deb http://mirrors.ustc.edu.cn/debian jessie main contrib non-free

deb http://mirrors.ustc.edu.cn/debian jessie-updates main contrib non-free

deb http://mirrors.ustc.edu.cn/debian-security/ jessie/updates main non-free contrib

deb http://mirrors.tuna.tsinghua.edu.cn/osmc/osmc/apt/  jessie main

#deb http://ftp.debian.org/debian jessie main contrib non-free

#deb http://ftp.debian.org/debian/ jessie-updates main contrib non-free

#deb http://security.debian.org/ jessie/updates main contrib non-free

#deb http://apt.osmc.tv jessie main
```

更改軟件源之後 請更新系統軟件源

```bash
# apt-get update -y && apt-get upgrade -y
```

之後本人推薦 Zsh 作爲系統默認 Shell 環境

使用一鍵開箱的 Zsh 的配置 [oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh)

通過 curl 安裝

```bash
$ sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
```

或通過 wget 安裝

```bash
$ sh -c "$(wget https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"
```

安裝完成按提示輸入用戶密碼替換系統當前 Shell 環境 如果沒有提升請手動設置 Zsh 爲默認 Shell 環境

```bash
$ chsh -s /bin/zsh
```

到此爲止 系統基礎的設置已經完成

接下來可以裝一下自己需要的功能 例如 screen、tree、htop、cow、shadowsocks 、 chinaDNS、xunlei-fastdick 等

OSMC 的更多高級設置請見 https://osmc.tv/wiki/general/configuration-file-locations/

切記：請勿隨意斷開電源！ 關閉電源前請執行關機命令 或 重啟命令

```bash
# poweroff
```

或

```bash
# reboot
```

 靜待系統關閉完成再拔掉電源連接線。