title: 『君の名は。』Chrome 主题
date: 2016-12-05 20:03:01
tags: Chrome
---
![](https://o3ziuzht9.qnssl.com/kiminonawa_1.png)

![](https://o3ziuzht9.qnssl.com/kiminonawa_2.png)


[安裝此主題樣式](https://www.themebeta.com/chrome/theme/633833) 

Chrome 擴展商店註冊費用太貴算了。

[Github](https://github.com/ky0ncheng/chrome-kiminonawa-theme)

本主題支持 5K(5120 × 2160) 分辨率顯示。

5K 版本請自行使用 chrome://extensions/ 載入擴展 kiminonawa5k。


<!-- more -->

-----

版權說明

使用素材來自 [Pixiv](http://www.pixiv.net/member_illust.php?mode=medium&illust_id=59085909)

背景圖像版權來自 [君の名は。](http://www.kiminona.com/index.html)