title: Google 的正確使用方法
id: 1810


date: 2014-06-22 15:34:47
tags:
---
![](http://segmentfault.com/img/bVcCug)

Google在大陸已經封了差不多有20天   訪問是極其的困難

##下面我會介紹幾種輕輕鬆鬆訪問Google的方法

你需要個靠譜的DNS服務器  比如 [https://code.google.com/p/openerdns/](https://code.google.com/p/openerdns/) 或者 [http://alidns.com/](http://alidns.com/)


推薦各位使用Chrome瀏覽器  對自己家的Google有神祕加成  [http://ippotsuko.com/open-google/](http://ippotsuko.com/open-google/)


或許你還需要[http://jingyun.org](http://jingyun.org)  這樣你就可以訪問其他受限制的網站了  支持全平臺 包括Windows,Mac,Linux,iOS,Android等等 甚至你家的路由器也可以用）

&nbsp;

## Google搜索服務優化

Google會根據你的地區自動切換搜索的語言   在大陸是自動跳轉到谷歌香港  你懂的  如果你要搜索一些奇奇怪怪的東西 是很難的 因爲google香港禁用了安全搜索 這個時候你需要 [https://google.com/ncr](https://google.com/ncr) 打開這個網址  Google就不會自動跳轉了    或者<span style="color: #222222;">進入</span>[https://encrypted.google.com](https://encrypted.google.com/)   解除google的搜索限制 這個是完全加密的

&nbsp;

Chrome瀏覽器可以用地址欄搜索    用ncr的參數應該是
<pre class="lang:default decode:true">https://google.com/#lr=lang_zh-CN&amp;newwindow=1&amp;q=%s</pre>
設置爲默認即可  如圖：

![](http://segmentfault.com/img/bVcCul)


以上都不行 也可以試試大神自己搭建的反向代理  [g.ttlsa.com](http://g.ttlsa.com) 或者 [wen.lu](http://wen.lu)

然後 你應該就可以正常的使用Google了~


 **登不上Google 怪我咯？**

![](http://sfault-image.b0.upaiyun.com/ab/e5/abe5e9c33ab4934c487b35818e746a09)
