---
title: 一些優秀的 RSS 訂閱源分享
date: 2016-05-25 16:06:31
tags:
---
![](https://o3ziuzht9.qnssl.com/google-reader-rip.jpg)

首先祭奠一下的已經去世的 Google Reader ，這個閱讀器自從我接觸 RSS 訂閱就用了很久。

RSS 是個很方便查看更新信息并閱讀的一種方式，很多網站都支持訂閱功能。

我的訂閱包括一些時事新聞頻道和一些科技媒體，還有一些非常優秀的個人博客。

個人推薦使用 [Inoreader](https://inoreader.com/)

也可以使用 [Feedly](https://cloud.feedly.com) / [Digg Reader](https://digg.com/reader) 等閱讀器導入訂閱備份文件即可。

![](https://o3ziuzht9.qnssl.com/import-xml.jpg)

OPML 訂閱備份文件 [下載](https://github.com/ky0ncheng/my-rss-subscriptions/raw/master/Inoreader%20Subscriptions%2020160525.xml)

<!--More-->