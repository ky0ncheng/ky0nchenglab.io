title: 新目標 新計劃~玩物喪志之樹莓派
tags:
  - Linux
  - Raspberry Pi
id: 124

  
date: 2013-07-08 01:40:56
---

![](http://ww4.sinaimg.cn/large/6d9bd6a5jw1e6erhgb7i4j20b40bj412.jpg)

好好存錢 好想買個樹莓派玩玩

![](http://ww4.sinaimg.cn/large/6d9bd6a5jw1e6erm22u5oj21000qwq7s.jpg)
![](http://ww1.sinaimg.cn/large/6d9bd6a5jw1e6ermcpmz4j21000qw7aa.jpg)
（圖片來自狐吧基佬）

![](http://ww3.sinaimg.cn/large/6d9bd6a5jw1e6ern9c844j20dw0aeac7.jpg)

![](http://ww3.sinaimg.cn/large/6d9bd6a5jw1e6erosorduj20qc0lzgs5.jpg)

最新的樹莓派B型配置：

*   芯片：Broadcom BCM2835
*   CPU：700 MHz ARM1176JZF-S core (ARM11 家族)
*   GPU：Broadcom VideoCore IV
*   視頻輸出：RCA, HDMI
*   音頻輸出：3.5 mm 耳機接口, HDMI
*   存儲方式: SD 卡
*   RJ45 接口百兆網卡
300塊不到  你可以拿它做任何你想做的事   監控   路由   小型服務器  DNS輔助解析  讀卡器（好奢侈的讀卡器。。。）  播放器   還能插SIM卡上網  除了這些主流功能 還有很多 非主流功能。。。

比如：
<object width="640" height="480" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0" align="middle"><param name="src" value="http://player.youku.com/player.php/sid/XNDA0OTcxNzcy/v.swf" /><param name="quality" value="high" /><param name="allowscriptaccess" value="sameDomain" /><param name="allowfullscreen" value="true" /><embed width="640" height="480" type="application/x-shockwave-flash" src="http://player.youku.com/player.php/sid/XNDA0OTcxNzcy/v.swf" quality="high" allowscriptaccess="sameDomain" allowfullscreen="true" align="middle" /></object>

&nbsp;

<object width="640" height="480" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0" align="middle"><param name="src" value="http://player.youku.com/player.php/sid/XNDMwMTQ2MTQw/v.swf" /><param name="quality" value="high" /><param name="allowscriptaccess" value="sameDomain" /><param name="allowfullscreen" value="true" /><embed width="640" height="480" type="application/x-shockwave-flash" src="http://player.youku.com/player.php/sid/XNDMwMTQ2MTQw/v.swf" quality="high" allowscriptaccess="sameDomain" allowfullscreen="true" align="middle" /></object>

<object width="640" height="480" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0" align="middle"><param name="src" value="http://player.youku.com/player.php/sid/XNDYwNjY4NzQ0/v.swf" /><param name="quality" value="high" /><param name="allowscriptaccess" value="sameDomain" /><param name="allowfullscreen" value="true" /><embed width="640" height="480" type="application/x-shockwave-flash" src="http://player.youku.com/player.php/sid/XNDYwNjY4NzQ0/v.swf" quality="high" allowscriptaccess="sameDomain" allowfullscreen="true" align="middle" /></object>

<object width="640" height="480" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0" align="middle"><param name="src" value="http://player.youku.com/player.php/sid/XNDMwNzM0ODUy/v.swf" /><param name="quality" value="high" /><param name="allowscriptaccess" value="sameDomain" /><param name="allowfullscreen" value="true" /><embed width="640" height="480" type="application/x-shockwave-flash" src="http://player.youku.com/player.php/sid/XNDMwNzM0ODUy/v.swf" quality="high" allowscriptaccess="sameDomain" allowfullscreen="true" align="middle" /></object>

**這些基礎上還可以做得出什麼?:**

*   增加身份驗證
*   加上聲音接收模塊,簡單的可以實現聲控開門,
*   更難一些的通過接收聲音二維碼開門.
*   加上攝像頭,可以當監控用,也許可以實現人臉識別~
*   加上指紋模塊,可以自己來實現指紋識別..
*   更NB也許還可以通過腳步聲,步態進行身份識別呢?
*   加上粉塵傳感器,還可以兼職PM2.5檢測器.其它什麼溫溼度壓力依此類推.
&nbsp;

樹莓派的使用也很簡單 對於一個熟練操作Linux系統的基本毫無壓力   你不習慣Linux  也可以裝個Andorid玩玩（安德猴也算是Linux的一種！！！ 囧（‘Д`）

我還是喜歡Arch ARM版+Xfce4  Ubuntu這個你就別想了  不支持這個ARM的指令集  樹莓派的CPU應該跑得動Firefox吧。。。不過512MB的RAM跑Chrome就顯得吃力了  日常的上網綽綽有餘了 作爲一個不“普通”的開發板  樹莓派確實物有所值

&nbsp;

本文參考：[http://yishanju.diandian.com/post/2013-03-05/40049129297](http://yishanju.diandian.com/post/2013-03-05/40049129297)

[http://www.geekpark.net/read/view/166143](http://www.geekpark.net/read/view/166143)

&nbsp;
