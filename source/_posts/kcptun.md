---
title: Kcptun 服務端與客戶端啟用筆記
date: 2016-08-09 15:47:44
tags: kcptun
---
![](https://github.com/xtaci/kcptun/raw/master/kcptun.png)

![](https://o3ziuzht9.qnssl.com/kcptun-logic.png)

原理圖

目前支持情況

Platform | Windows | macOS | Linux | Android | iOS
----|------|----|----|----|----
cli | O  | O | O | O | X
gui | O(第三方) | X | X | O | X
x86 | O  | O | O | O | X
x64 | O  | O | O | O | X
arm5 | n/a  | n/a | O | O | n/a
arm6 | n/a  | n/a | O | O | n/a
arm7 | n/a  | n/a | O | O | n/a
arm8 | n/a  | n/a | use arm7 | O | n/a
mips32 | n/a  | n/a | O | n/a | n/a
mips64 | n/a  | n/a | O | n/a | n/a

<!-- more -->
主要參數優化

```
適用大部分ADSL接入（非對稱上下行）的參數（實驗環境電信100M ADSL）
其它帶寬請按比例調整，比如 50M ADSL，把 CLIENT 的 -sndwnd -rcvwnd 減掉一半，SERVER 不變

SERVER:   -mtu 1400 -sndwnd 2048 -rcvwnd 2048 -mode fast2
CLIENT:   -mtu 1400 -sndwnd 256 -rcvwnd 2048 -mode fast2 -dscp 46
*巭孬嫑亂動* 
```


啟動服務器端

```bash
./server_linux_amd64 -t "server_ss_ip:1080" -l "local_ip:554" -mtu 1400 -sndwnd 2048 -rcvwnd 2048 -mode fast2
```

`阿里雲專有網絡 Virtual Private Cloud 用戶，請使用 local_ip:0.0.0.0 作為監聽端口`

啟動客戶端

```bash
./client_darwin_amd64 -r "serevr_ip:554" -l "lcoal_ip:2333" -mtu 1400 -sndwnd 256 -rcvwnd 2048 -mode fast2 -dscp 46
```

`樹莓派3 用戶 請暫時使用 armv7 預編譯二進制文件`

如果客戶端還使用了 Shadowsocks 代理，可以設置 `localhost:2333` 作為 kcptun 中繼。

```conf
{
    "server":"127.0.0.1",
    "server_port":2333,
    "local_address": "127.0.0.1",
    "local_port":1080,
    "password":"shadowsocks_passwd",
    "timeout":300,
    "method":"rc4-md5",
    "fast_open": true,
    "workers": 1,
    "prefer_ipv6": true
}
```

參數說明

```
USAGE:
   server_linux_amd64 [global options] command [command options] [arguments...]

VERSION:
   20160808

COMMANDS:
     help, h  Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --listen value, -l value  kcp server listen address (default: ":29900")
   --target value, -t value  target server address (default: "127.0.0.1:12948")
   --key value               key for communcation, must be the same as kcptun client (default: "it's a secrect") [$KCPTUN_KEY]
   --crypt value             methods for encryption: aes, tea, xor, none (default: "aes")
   --mode value              mode for communication: fast3, fast2, fast, normal (default: "fast")
   --mtu value               set MTU of UDP packets, suggest 'tracepath' to discover path mtu (default: 1350)
   --sndwnd value            set send window size(num of packets) (default: 1024)
   --rcvwnd value            set receive window size(num of packets) (default: 1024)
   --nocomp                  disable compression
   --datashard value         set reed-solomon erasure coding - datashard (default: 10)
   --parityshard value       set reed-solomon erasure coding - parityshard (default: 3)
   --dscp value              set DSCP(6bit) (default: 0)
   --help, -h                show help
   --version, -v             print the version
```

附：

[kcptun 官方手冊](https://github.com/xtaci/kcptun/blob/master/README.md)
[kcptun 預編譯二進制下載](https://github.com/xtaci/kcptun/releases)
[kcptun 啟動腳本](https://github.com/ky0ncheng/kcptun-script)
[kcptun GUI for windows](http://www.dou-bi.com/ss-jc37/)
[kcptun for Android 使用說明](http://www.dou-bi.com/ss-jc38/)
[kcptun for mips32 交叉編譯](https://github.com/xtaci/kcptun/wiki/mip32-cross-compile)