title: 《紳士大概一分鐘》學日語時間
date: 2015-10-29 16:10:23
tags: 日本語
---
![](https://o3ziuzht9.qnssl.com/wanle.jpg)

把山下智博君的《紳士大概一分鐘》的 Youtube 播放列表看完了

特地把裡面的日語學習內容整理了一下 （好多奇奇怪怪的東西XD

<!-- more -->

しょぼい

sho bo i

一點都不厲害

形容讓人失望的不顯眼的狀態

この携帯本当にしょぼい

このけいたいほんとうにしょぼい

Kono keitai hontouni shoboi

這手機一點都不好用

------

彼の技術はまジしょぼい。

かれのぎじゅつはまジしょぼい

Kareno gijutsu hamaji shoboi

他的技术真让人失望

------

自業自得

じごうじとく

ji gou ji toku

自作自受

爲自己做的事情帶來的後果自己承擔。

-----

通報しますた

つうほうしますた

tsuuhou shimasuta

已報警

------

パンスト破りたい

パンストやぶりたい

pansuto yaburitai

我要撕破連褲襪

---

美女の残り湯

びじょののこりゆ

bijono nokoriyu

美女的洗澡水

残り湯 指的是 泡過一次澡之後留下來的熱水

---

俺は新品　あいつわちゅうこひん

おれわしんぴん　あいつわちゅうこひん

orewashinpin aitsuwachuukohin

我是新品 他是二手貨

----

小学生と遊びたい

しょうがくせいとあそびたい

shougakusei asobitai

想和小學生玩

-----

秘宝館

ひほうかん

祕寶館

---

いや、これわアートだ

iya korewa a-to da

不，這是藝術。

----

真顔で何言ってるんですか？

まがおでなにいってるんですか？

magaode nani itte run desuka?

一本正經的胡說八道

----

赤巻紙

あかまきがみ

青巻紙

あおまきがみ

黄巻紙

きまきがみ

----

ん？何が欲しいか言ってごらん？

ん？なにかほしいかいってごらん？

n? nanika hoshii ka itte goran?

恩，你說，你想要什麼？

---

いい加減にしろ

いいかげんにしろ

iikangennishiro

你夠了

----

人わ見た目が九割引

ひとわみためがきゅわり

hitowamitamegakyu-wari

成敗百分之九十看臉

---

但し、イケメンに限る

ただし、イケメンにかぎる

tadashi ikemennikagiru

看臉

----

合羽橋

カッパばし

kappabashi

動画屋筋商店街

どうがやすじしょうてんが

dougayasujishoutenga

---

じゅるり

jururi

よだれが止まらない

よだれがやまらない

todareyamarinai

口水停不下來

ウホツ

Uho

表示驚訝

ゴグリ

gokuri

咕嚕 吞食物的擬聲詞

---

外なまら寒いから

そとなまらさむいから

satonamarasamuikara

外面很冷 (北海道日語方言)

---

直視できない

ちょくしできない

chokushidekinai

無法直視

画面が美しいすぎて直視できない

がめんがうつくしすぎてちょくしてきない

gamenga utsukushii sugitee chokushidekinai

畫面太美無法直視

----

何かが違う

なにかがちがう

nanikagachigau

哪裡不對？

---

違和感仕事しろ

いわかんしごとしろ

違和感君你快出現吧

----

白い液体

しろいえきたい

shiroiekitai

白色液體

---

立たない

たたない

tatanai

立不起來

大事な時に、立たない

だいじなときに、たたない

daijinatokini tatanai

關鍵時候立不起來

---

はしゃぎすぎ

hashagisugi

玩脫了

---

無茶ぶり

むちゃぶり

無理取鬧

---

来たこれ

kitakore

ktkr　(2ch用語 形容非常開心)

---

いけぶくろ

ikebukuro

池袋　東京地名

---

すみません

simimasen

打擾一下

はい、少々お待ちください

はい、しょうしょうおまちください

hai、shoushouomachikudasai

好，稍等一下

すみませんお待たせしました、お伺いいたします

すみませんおまたせしました、おうかがいいたします

sumimasenomataseshimashita、oukagaiitashimasu

讓你久等了，請問你要些什麼？

はい、かしこまりました

hai、kashikomarimashita

好的、我知道了

---

何のい因果だよ！?

なにのいんがだよ！?

什麼仇什麼怨！?

---

鉄也のアレ、鉄のように硬いね

てつやのあれ、てつのようにかたいね

tetsuyanoare、tetsunoyounikataine

鐵也 的那個，像鐵一樣硬哦

---

視聴者ペレぜント

しちょうしゃペレせんと

shichousyaperesento

觀衆福利

---

髪が後退しているのてわない、私が前進しているのだ

かみがこうたいしているのいたわない、わたしがぜんしんしてるのだ

kamigakoutaishiteirunoitawanai、watashigazenshinshiterunoda

並不是我的頭髮向後退了，而是我在向前進

---

小麦粉か何かだ

こむぎこかなにかだ

komugikokananikana

這個是小麥粉什麼的

---

それってお高いんでしょ？

それっておたかいんてしょ？

soretteotakashiidesho?

這個東西看上去有點貴

---

これが現実だ

これがげんじつだ

koregagenjitsuda

這就是現實啊

---

シマパン

shimapan

斑馬條紋內褲

しまうま

shimauma

斑馬

---

コオル

kooru

乾杯

---

アイツ
無茶しやがって

アイツ
むちゃしやがって

aitsu
wuchashiyagatte

那小夥子亂搞一通

---

メシウマ

meshiuma

幸災樂禍

---

山下君っていい人だよね

やましたくんっていいひとだよね

yamashitakuntteiihitodayone

山下君你是個好人

---

クンカ　クンカ

kunakuna

問

---

初めてだから優しくしてね

はじめてだからやさしくしてね

hanemetedakarayasashikushitene

我是第一次對人家溫柔一點哦

---

無理ゲーせぎて笑える

むりげーすぎてわらえる

murigesugitewaraeru

完全做不到 只能呵呵呵

---

ヤバイ

yabai

糟糕

---

クソ ワロタ

kuso warota

笑尿了

---

店長　やめてください

てんちょう　やめてください

tenchou yametekudasai

店長 不要這樣

---

婚活

こんかつ

konkatsu

相親活動

---

いとおかし

itookashi

好評

---

ﾌｱーー！

fa

表示驚訝

---

よいお年を

よいおとしを

yoiotoshiwo

新年快樂(過年前使用)

---

あにき

兄貴

哥哥 或 肌肉男

---

君に似合うと思って

きみににあうとおっもって

kimininiautoomotte

我想很適合你

---

金持ちはワガママ

かねもちわワガママ

kanemochiwawagamama

有錢任性

---

お巡りさん　このひとです

おまわりさん　このひとです

omawarisan konohitodesu

警察叔叔 就是這個人

---

しんどい

shindoi

心累

受験勉強マジでしんどい

じゅけんべんきょうまじでしんどい

jukenbenkyoumajideshindoi

高考真的吃力

あの人と話すのマジでしんどい

あのひととはなすのまじでしんどい

anohitotohanasunomajideshindoi

跟那個人談話真的痛苦

こんなしんどい仕事嫌だよ

こんあしんどいしごといやだよ

konnashindoishigotoiyadayo

我討厭這麼費勁的工作
