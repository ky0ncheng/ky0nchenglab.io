title: Cubieboard 使用感受
id: 1460

  
date: 2014-03-08 18:38:56
tags:
---

Cubieboard，簡稱Cb，是一個由珠海的Cubietech團隊推出，採用全志A10/A20處理器的開發板，這個處理器大家可能很熟悉，不就是pcduino用的處理器嘛，且慢，pcduino用的是單核A10，新版的Cubieboard已經用 雙核的A20了，這個處理器相對於較早推出的樹莓派的博通處理器而言，這個處理器頻率更高（據傳可達1.5Ghz，不加壓可達1.2Ghz），性能也更好一些，類似的產品還有pcDuino和MK802，Cubieboard常用的範圍是作爲一個安卓電視盒和一個Linux迷你PC進行使用，可以選配美麗的外殼，有亞克力的和塑料的，裝起來就像一個電視盒子了，不過我一般不裝外殼，但是我建議大家最好裝一下，避免放到金屬物體上短路了。
![](http://cubieboard.org/wp-content/uploads/2012/09/cubieboard_top.jpg)

&nbsp;

![](http://cubieboard.org/wp-content/uploads/2012/09/cubieboard_side3.jpg)

![](http://cubieboard.org/wp-content/uploads/2012/09/cubieboard_side4.jpg)

&nbsp;

![](http://cubieboard.org/wp-content/uploads/2012/09/cubieboard_in_hand.jpg)

![](http://cubieboard.org/wp-content/uploads/2012/09/cubieboard_coin2.jpg)

![](http://cubieboard.org/wp-content/uploads/2012/09/cubieboard_back1.jpg)

<span style="line-height: 1.5em;">我的是Cubieboard2  它</span>的處理器是雙核A20，而Cubieboard1的是單核A10

Cubieboard1有分先後兩個版本，一個是2012年08月08日開發的，稱爲08-08版，另一個則是2012年09月09日開發的，稱爲09-09版，現在市面上大多是09-09版的，08-08版的我沒有，也不知道詳細的數據，關於怎麼辨別是08-08版還是09-09版的，只需看一下電路板正面的白色文字即可
提示：因爲Cubieboard2和1的區別僅在於更換了處理器，所以電路板上的文字還是09-09，因此辨別Cubieboard是1還是2的方法是看處理器上的灰色激光打印文字，如果是A20則是Cubieboard2，反之亦然

![](http://ww1.sinaimg.cn/large/6d9bd6a5jw1ee8i6uvdenj21kw16odzo.jpg)

![](http://ww1.sinaimg.cn/large/6d9bd6a5jw1ee8g56zs12j21kw23u4qp.jpg)

![](http://ww4.sinaimg.cn/large/6d9bd6a5jw1ee8e8arwb6j21kw16oqpk.jpg)

&nbsp;

我身邊有這些配件  RJ45網線 x1  ， HDMI 連接線x1 ，Sandisk 8G SD卡 x1 ，mini USB連接線 ，愛國者充電寶

我還需要一個5v@2a的usb電源轉換器。。。。現在沒有隻能用充電寶輸出2a的電流了 不然hdmi無法正常工作

&nbsp;

我的這個cb預裝的 lubuntu  我先是用刷sd做啓動盤  發現有點問題 就直接把預裝在nand裏的lubuntu換成android了   等@secdns 放出.tar包 再換回來  當作家裏的dns服務器

&nbsp;

把cubieboard變成小米盒子、天貓盒子這種東西 你僅僅需要幾個東西即可

先連接電腦   推薦win7系統  其他系統上的工具不是很穩定

用PhoenixSuit 1.0.6（[http://pan.baidu.com/s/1kTBgctT](http://pan.baidu.com/s/1kTBgctT)）  把android 4.2固件（[http://pan.baidu.com/s/1ntsfj3z](http://pan.baidu.com/s/1ntsfj3z)） 刷在cubieboard的nand上

root請下載[super su](https://play.google.com/store/apps/details?id=eu.chainfire.supersu) 安裝即可管理權限  （play商店的apk可以用[這個](http://apps.evozi.com/apk-downloader/)提取

桌面你可以用[兔子桌面](http://www.tuziv.tv/d)   [沙發桌面 ](http://app.shafa.com/view/com.shafa.launcher)也可以用[apex啓動器](https://play.google.com/store/apps/details?id=com.anddoes.launcher)

電視app  推薦  [vst全聚合](http://www.91vst.com/)   [泰捷視頻](http://www.togic.com/livetv)   [愛奇藝](http://app.iqiyi.com/tv/player/)  [迅雷看看](http://www.kankan.com/app/android_tv.html)  [優酷tv](http://mobile.youku.com/index/wireless)

&nbsp;

&nbsp;

&nbsp;

感謝@[珠海GDG](http://weibo.com/gdgzh) @[上海GDG](http://weibo.com/shgdg)  @[Shaman_s](http://weibo.com/336155573)  給我的cubieboard2 硬件設備  ↖(￣▽￣")

&nbsp;

&nbsp;
