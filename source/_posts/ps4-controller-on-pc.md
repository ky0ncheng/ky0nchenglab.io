title: PS4 手柄有線/藍牙無線連接PC
date: 2015-03-28 20:49:01
tags: Playstation4 
---
![](https://ssl.daoapp.io/ww3.sinaimg.cn/large/6d9bd6a5gw1eqlqcnpkucj21kw16o4h2.jpg)

本文將講解Windows 7系統下如何將PS4手柄與PC電腦配對，並模擬成Xbox 360手柄的設置

<!-- more --> 
需要無線連接之前請先嚐試有線連接是否正常

* 有線連接

拿一條安卓設備常見的microusb連接線   連接PC等待安裝完驅動即可使用
但此時是兼容的DirectInput遊戲手柄設備 PS4手柄背部此時LED顯示爲黃色常亮

需要去這個論壇下載 [DS4 TO XInput Wrapper](http:///forums.pcsx2.net/Thread-DS4-To-XInput-Wrapper) 這個軟件模擬XBOX遊戲手柄 *最新版本是DS4TOOL 1.2.2.zip

下載完成之後解壓壓縮包  

打開Virtual Bus Driver子目錄

![](https://ssl.daoapp.io/ww3.sinaimg.cn/large/6d9bd6a5gw1eqlotemroxj20r00idadq.jpg)

安裝ScpDriver.exe

![](https://ssl.daoapp.io/ww1.sinaimg.cn/large/6d9bd6a5gw1eqlov6vfmnj20go0bm0u2.jpg)

然後返回解壓之後的文件根目錄  打開ScpServer.exe
這時主程序最小化到任務欄右下角去了

連接正常 PS4手柄背部LED燈顯示藍色常亮
DS4TOOL會顯示手柄的mac地址  內置電池電量  和觸控板開啓狀態

如果沒有正常連接 則沒有顯示  需要重新插拔設備即可

![](https://ssl.daoapp.io/ww3.sinaimg.cn/large/6d9bd6a5gw1eqloyh86ewj20m209vgpt.jpg)

DS4TOOL支持手柄的更多設置（反正默認夠用

------

熱鍵說明

L2+按下觸摸板 | 關閉觸摸板                     
L2+R2+按下觸摸板 | 開啓觸摸板 
雙指上滑/下滑 | 向上/向下滾動  
單擊觸摸板 | 相當於鼠標左鍵  
觸摸板右側單擊後左側單擊 | 相當於鼠標右鍵  
PS鍵+Option鍵 | 關閉藍牙  

--------

* 無線設置

如果是搭載了Windows 7/Windows 8/8.1的筆記本  可以打開系統藍牙設置並配對

如果是臺式機 請買個支持藍牙2.1+EDR的適配器  例如:[ORICO 奧睿科 BTA-402 藍牙V4.0 適配器](https://www.amazon.cn/ORICO-%E5%A5%A5%E7%9D%BF%E7%A7%91-BTA-402-%E8%93%9D%E7%89%99V4-0-%E9%80%82%E9%85%8D%E5%99%A8-USB2-0%E8%93%9D%E7%89%99%E6%8E%A5%E6%94%B6%E5%99%A8%E6%94%AF%E6%8C%81Windows-P-Vista-7-8/dp/B00AKO7XOW/ref=sr_1_1?ie=UTF8&qid=1427546082&sr=8-1&keywords=%E8%93%9D%E7%89%99%E9%80%82%E9%85%8D%E5%99%A8)

*本文以此適配器作爲演示

插入主板的USB2.0接口  等待安裝驅動完成

右鍵底部狀態欄的藍牙圖標 搜索新設備

![](https://ssl.daoapp.io/ww3.sinaimg.cn/large/6d9bd6a5gw1eqlq2yvmdfj20hg0eh755.jpg)

此時 按住手柄的PS鍵和Share鍵 此時PS手柄背部的LED燈顯示白色並閃爍 開始配對

![](https://ssl.daoapp.io/ww4.sinaimg.cn/large/6d9bd6a5gw1eqlq6ak2cpj20ff0b73zk.jpg)

等待配對完成  此時PS手柄背部的LED燈顯示白色常亮

![](https://ssl.daoapp.io/ww3.sinaimg.cn/large/6d9bd6a5gw1eqlq6ogn1ij20m209v774.jpg)


此時打開DS4TOOL可以正常顯示設備狀態 
具體手柄設置參考上文 以上～

