title: 謹賀新年2015
date: 2015-02-18 22:03:30
tags:
---
![](https://ssl.daoapp.io/ww4.sinaimg.cn/large/6d9bd6a5gw1epduwrxgluj20go0p0acd.jpg)
<div align=right> <sup>Photograph by Emma Su in <a href="https://chinesenewyear.withgoogle.com/gallery?hl=zh-CN#!/!5232414909530112">Google</a></sup></div>

生肖更迭，辭舊迎新，又是一年團聚時。

世上再美的風景也不及回家的那段路，外面的世界再精彩，也不及團圓時分帶來的留戀。

願新年  吉祥如意 平安喜樂

<div align=right>甲午年  除夕寄語</div>  