title: Yunpian
date: 2016-07-19 00:07:29
---
![](https://o3ziuzht9.qnssl.com/api-wiki-index-01.png)
雲片 API 開發文檔 Material Desgin 視覺設計初稿

![](https://o3ziuzht9.qnssl.com/landingPage.png)
雲片 Landing Page 視覺 Flat 化設計初稿 (插圖未重畫)

![](https://o3ziuzht9.qnssl.com/idCard_Chuyufeng-01.png)
雲片員工工牌

![](https://o3ziuzht9.qnssl.com/weixin_footer-01.png)
雲片微信公眾號文章 Footer

![](https://o3ziuzht9.qnssl.com/yunpian-Flyer-02.png)
雲片宣傳 Flyer 正面

![](https://o3ziuzht9.qnssl.com/yunpian-Flyer-01.png)
雲片宣傳 Flyer 反面

![](https://o3ziuzht9.qnssl.com/yp-tee.png)
雲片Tee

![](https://o3ziuzht9.qnssl.com/yp-ucloud-banner.png)
雲片 U 市場 Banner