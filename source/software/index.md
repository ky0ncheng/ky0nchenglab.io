title: Software
date: 2016-08-19 00:07:29
---
# Windows 10 X64

zh-CN
```
ed2k://|file|cn_windows_10_multiple_editions_version_1607_updated_jul_2016_x64_dvd_9056935.iso|4347183104|35EA5DB0F3BB714F5CE0740FB89D82D1|/
```

en-GB
```
ed2k://|file|en_windows_10_multiple_editions_version_1607_updated_jul_2016_x64_dvd_9058187.iso|4380387328|870E0589EBEC3296745462E8ACA53FB2|/
```


# Windows Server 2016 X64

zh-CN
```
ed2k://|file|cn_windows_server_2016_x64_dvd_9718765.iso|6176450560|CF1B73D220F1160DE850D9E1979DBD50|/
```

en-GB
```
ed2k://|file|en_windows_server_2016_x64_dvd_9718492.iso|5883301888|1EC2F6E9D91E2D47A175C5CC162574EA|/
```



# Office 2016

Windows

```
ed2k://|file|cn_office_professional_plus_2016_x86_x64_dvd_6969182.iso|2588266496|27EEA4FE4BB13CD0ECCDFC24167F9E01|/
```

macOS

```
http://officecdn.microsoft.com/pr/C1297A47-86C4-4C1F-97FA-950631F94777/OfficeMac/Microsoft_Office_2016_Installer.pkg
```


<div style="display:none">https://ojtiignjd.qnssl.com/Office%20for%20Mac%202016%20License%20Installer.zip</div>


# Chrome

[For Windows X64](https://dl.google.com/tag/s/appguid%3D%7B8A69D345-D564-463C-AFF1-A69D9E530F96%7D%26iid%3D%7BD5C6EFEE-0349-9C32-6B7A-58D53483B1D7%7D%26lang%3Dzh-TW%26browser%3D4%26usagestats%3D0%26appname%3DGoogle%2520Chrome%26needsadmin%3Dprefers%26ap%3Dx64-stable/update2/installers/ChromeStandaloneSetup64.exe)

[For macOS](http://dl.google.com/chrome/mac/stable/GGRO/googlechrome.dmg)

[For Linux X64 .DEB](http://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb)

[For Linux X64 .RPM](http://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm)

# Firefox

Offline installer download, Please visit https://www.mozilla.org/en-US/firefox/all/

# Internet

[Spechtlite For macOS](https://github.com/zhuhaow/SpechtLite/releases/download/0.6.0/SpechtLite.zip)

[SS For macOS](https://github.com/shadowsocks/ShadowsocksX-NG/releases/download/1.3.2/ShadowsocksX-NG-1.3.2.zip)

Python Version : `https://github.com/shadowsocks/shadowsocks/archive/master.zip`

[SS For Windows](https://github.com/shadowsocks/shadowsocks-windows/releases/download/3.3.5/Shadowsocks-3.3.5.zip)

[SS For Android](https://github.com/shadowsocks/shadowsocks-android/releases/download/v3.3.0/shadowsocks-nightly-3.3.0.apk)

[SS For iOS](https://itunes.apple.com/cn/app/wingy-smart-proxy-for-http/id1148026741?mt=8)

<div style="display:none">https://ojtiignjd.qnssl.com/KMSAuto_Net_2016_v1.4.8_Portable.zip</div>

<div style="display:none">https://ojtiignjd.qnssl.com/ThunderSpeed1.0.34.360.exe</div>

<div style="display:none">https://ojtiignjd.qnssl.com/Adobe%20zii%202.2.1.dmg</div>

<div style="display:none">https://ojtiignjd.qnssl.com/AxureRP8_3312_Setup.exe</div>

<div style="display:none">https://ojtiignjd.qnssl.com/AxureRP8_3312_Setup_pw_xclient.info.dmg</div>

<div style="display:none">https://ojtiignjd.qnssl.com/amtemu.v0.9.2.win-painter.zip</div>

<div style="display:none">https://ojtiignjd.qnssl.com/axure_key.txt</div>

<div style="display:none">https://ojtiignjd.qnssl.com/Sketch_41.2_xclient.info.dmg</div>

