title: uname -a
date: 2016-03-30 00:07:29
---
<br>

#### whoami

Kyon Cheng

索尼大法好

港樂愛好者

業餘 UI/UX Designer.

語言 [zh-wuu](https://zh.wikipedia.org/wiki/%E5%90%B4%E8%AF%AD) 

<br>

#### sns

[Twitter](https://twitter.com/ky0ncheng/)

[Instagram](https://www.instagram.com/ky0ncheng/)

[GitHub](https://github.com/ky0ncheng)

[Dribbble](https://dribbble.com/ky0ncheng)

[Steam](https://steamcommunity.com/id/ky0ncheng)

[PSN](https://my.playstation.com/ky0ncheng)

[Origin](http://battlelog.battlefield.com/bf4/user/ky0ncheng/)

<br>

#### traceroute

[GDG Shanghai](https://plus.google.com/+Gdgshanghai)

[SHLUG](https://twitter.com/shanghailug)

[GitCafe](https://gitcafe.com)

[AOSC](https://aosc.io)

<br>

#### mail

[ky0ncheng\[at\]protonmail.ch](mailto:ky0ncheng@protonmail.ch)

<br>

#### bitcoin

1AbVLoqLrouB9HNgPhKbDSmwQXy8pPqaVT

<br>

#### donate

歡迎註冊並使用下方推薦的 VPS 服務商

[Linode](https://www.linode.com/?r=2453330f833950bb646c3c0e1138cf1c058334f0)

[Vultr](http://www.vultr.com/?ref=6901949)

<br>

#### echo

本博客採用 [署名-非商業性使用-相同方式共享 4.0 協議](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh_TW) 進行許可 
This site is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)
