title: BanmaTrip
date: 2016-07-19 00:07:29
---
目前的斑馬旅遊後臺v2.2

客戶訂單管理頁面

![](https://o3ziuzht9.qnssl.com/Screen%20Shot%202017-02-23%20at%205.48.37%20PM.png)

旅遊資源採購頁面,用於內部採購和對賬確認

![](https://o3ziuzht9.qnssl.com/Screen%20Shot%202017-02-23%20at%205.48.43%20PM.png)

旅遊產品頁面,用於整合旅遊資源,管理或製作各種旅遊線路

![](https://o3ziuzht9.qnssl.com/Screen%20Shot%202017-02-23%20at%205.48.30%20PM.png)

旅遊產品製作,庫存設定,物品關聯,升級包匹配

![](https://o3ziuzht9.qnssl.com/Screen%20Shot%202017-02-23%20at%206.04.21%20PM.png)

旅遊資源庫存管理界面

![](https://o3ziuzht9.qnssl.com/Screen%20Shot%202017-02-23%20at%205.48.22%20PM.png)

簡化後的斑馬旅遊後臺v3,迭代版本

![](https://o3ziuzht9.qnssl.com/Desktop%20HD.png)

iOS 客戶端功能展示 

首頁

![](https://o3ziuzht9.qnssl.com/01_%E9%A6%96%E9%A1%B5.png)

旅遊私人訂制路線

![](https://o3ziuzht9.qnssl.com/01_%E7%A7%81%E4%BA%BA%E5%AE%9A%E5%88%B6.png)

Android 客戶端交互重新設計,迭代版本

![](https://o3ziuzht9.qnssl.com/bmandroid.png)

日本旅遊線路司導資源整合系統,迭代版本

需求單發佈

![](https://o3ziuzht9.qnssl.com/Screen%20Shot%202017-02-23%20at%206.10.01%20PM.png)

司導管理

![](https://o3ziuzht9.qnssl.com/Screen%20Shot%202017-02-23%20at%206.10.25%20PM.png)

需求單管理

![](https://o3ziuzht9.qnssl.com/Screen%20Shot%202017-02-23%20at%206.09.31%20PM.png)

需求單審核

![](https://o3ziuzht9.qnssl.com/Screen%20Shot%202017-02-23%20at%206.10.13%20PM.png)

司導端界面,微信平台

![](https://o3ziuzht9.qnssl.com/Screen%20Shot%202017-02-23%20at%206.10.41%20PM.png)

