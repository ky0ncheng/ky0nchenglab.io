title: DNS
date: 2016-08-19 00:07:29
---
# 中國科技大學無污染 USTC DNS (Mainland China)

202.141.176.93

202.141.162.123

202.38.93.153

202.38.93.94

# OneDNS (Mainland China)

112.124.47.27

114.215.126.16

42.236.82.22

# OpenDNS (Mainland China)

112.124.47.27

114.215.126.16

42.236.82.22

# 阿里雲 DNS (Mainland China)

223.5.5.5

223.6.6.6

# Dnspod Public DNS (Mainland China)

119.29.29.29

182.254.116.116

# 有線寬頻 i-Cable DNS (Hong Kong)

61.10.0.130

61.10.1.130

203.168.223.201

210.80.60.1

210.80.60.2

# 香港寬頻 Hong Kong BroadBand Network DNS (Hong Kong)

203.80.96.10

203.80.96.9

203.186.94.22

203.186.94.20

# NetFront 前線 (Hong Kong)

202.81.252.1

202.81.252.2

# HKNet 寬頻 DNS (Hong Kong)

202.67.240.222

202.67.240.221

# 和記環球電訊 DNS (Hong Kong)

210.0.255.250

210.0.255.251

210.0.128.250

210.0.128.251

202.45.84.67

202.45.84.68

202.181.203.251

202.181.203.252

202.181.203.253

202.181.203.254

202.181.240.44

# Pacific SuperNet (Hong Kong)

202.14.67.4

202.14.67.1

# So-net HK (Hong Kong)

203.99.142.8

203.99.142.9

# PCCW / Powerbase (Hong Kong)

202.153.97.2

202.153.97.130

# 數碼通寬頻 SmarTone BroadBand (Hong Kong)

202.140.96.51 

202.140.96.52 
 
# CPCNet Hong Kong Limited (Hong Kong)

202.76.4.1

202.76.4.2

# KDDI Hong Kong Limited (Hong Kong)

202.177.2.2

202.177.2.3

# iAdvantage Limited (Hong Kong)

202.85.128.32

202.85.128.33

203.194.239.32

202.85.170.89

# CyberExpress (Hong Kong)

202.85.146.104

202.60.252.8

# SpeedLink (Hong Kong)

202.134.93.120

202.65.151.85

# Pacific SuperNet (Hong Kong)

202.14.67.4

202.14.67.14

# HiNet DNS (Taiwan)

168.95.1.1

168.95.192.1

168.95.192.2

# Google Public DNS (Global)

8.8.8.8

8.8.4.4


2001:4860:4860:0:0:0:0:8888

2001:4860:4860:0:0:0:0:8844

# Open DNS (Global)

208.67.222.222

208.67.220.220

208.67.222.220

208.67.220.222


2620:0:ccc::2

2620:0:ccd::2


----------

# NTP SERVER

0.asia.pool.ntp.org
1.asia.pool.ntp.org
2.asia.pool.ntp.org
3.asia.pool.ntp.org

cn.pool.ntp.org
1.cn.pool.ntp.org
2.cn.pool.ntp.org
3.cn.pool.ntp.org
0.cn.pool.ntp.org

hk.pool.ntp.org
0.hk.pool.ntp.org
1.hk.pool.ntp.org
2.hk.pool.ntp.org
3.hk.pool.ntp.org

tw.pool.ntp.org
0.tw.pool.ntp.org
1.tw.pool.ntp.org
2.tw.pool.ntp.org
3.tw.pool.ntp.org

time1-7.aliyun.com
time.pool.aliyun.com

time.asia.apple.com
time.apple.com


[Help in Chinese](http://www.onedns.net/index.php?nfssp=setup)

[Help in English](https://developers.google.com/speed/public-dns/docs/using)